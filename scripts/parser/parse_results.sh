#!/bin/bash

mkdir ../../results

python3 slurm_out_parser.py
python3 slurm_out_dynamic_parser.py
python3 slurm_out_greedy_parser.py
python3 slurm_out_interval_parser.py
