results_path = "../../tgindex/cmake-build-release/"

output_path = '../../results/'

datasets = [
    "infectious",
    "sx-askubuntu-a2q",
    "digg",
    "prosper",
    "arxiv",
    "youtube",
    "sx-stackoverflow-a2q",
]


def read_file(filename):
    with open(filename) as f:
        content = f.readlines()
    data = [x.strip() for x in content]
    return data
