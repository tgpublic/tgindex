import pandas as pd

from os import listdir
from os.path import isfile, join

from common import read_file, results_path, datasets, output_path


def get_files(name):
    files = [f for f in listdir(results_path) if isfile(join(results_path, f))]
    result = []
    for f in files:
        if name in f and ("greedy" in f):
            result.append(f)
    return result


def get_files_baselines(name):
    files = [f for f in listdir(results_path) if isfile(join(results_path, f))]
    result = []
    for f in files:
        if name in f and ("baseline" in f):
            result.append(f)
    return result


def get_times(files, b):
    for f in files:
        if b in f:
            data = read_file(results_path + f)
            print(data)


def getdataexp1(file):
    data = read_file(results_path + file)
    name = file.split('_')[2]
    if "." in name:
        name = name.split('.')[0]
    x = {
        'dataset': name,
        'm': 0,
        'b': 0,
        'greedy_indexing_time': 0,
        'greedy_index_size': 0,
        'ssmin': 0,
        'ssmed': 0,
        'ssmax': 0,
        'ssavg': 0,
        'ssstdev': 0,
        'less_than_med': 0,
        'rnd_ea_time': 0,
        'rnd_fp_time': 0,
        'rnd_sp_time': 0,
        'hd_ea_time': 0,
        'hd_fp_time': 0,
        'hd_sp_time': 0
    }
    for pos in range(0, len(data)):
        line = data[pos]
        if "set memblock_size " in line:
            m = line.split("set memblock_size ")[1]
            x['m'] = int(m)
        if "set num buckets " in line:
            b = line.split("set num buckets ")[1]
            x['b'] = int(b)
        if "^^computed substream index" in line:
            before = float(data[pos - 1].split(': ')[1])
            x['greedy_indexing_time'] = before
        if "min, med, max: " in line:
            vals = line.split("min, med, max: ")[1].split(" ")
            x['ssmin'] = int(vals[0])
            x['ssmed'] = int(vals[1])
            x['ssmax'] = int(vals[2])
        if "avg. bucket size: " in line:
            vals = line.split("avg. bucket size: ")[1].split(" +- ")
            x['ssavg'] = float(vals[0])
            x['ssstdev'] = float(vals[1])
        if "mem. total size: " in line:
            b = line.split("mem. total size: ")[1].split(" ")[0]
            x['greedy_index_size'] = float(b)
        if "num vertices in substream with size less than median: " in line:
            b = line.split("num vertices in substream with size less than median: ")[1].split(" ")[0]
            x['less_than_med'] = int(b)
        if "Run earliest path substream+timeskip" in line:
            t = 'oot'
            if not "DUE TO TIME LIMIT" in (data[pos + 1]):
                t = float(data[pos + 1].split(': ')[1].split(" ")[0])
            if x['rnd_ea_time'] == 0:
                x['rnd_ea_time'] = t
            else:
                x['hd_ea_time'] = t
        if "Run fastest path substream+timeskip" in line:
            t = 'oot'
            if not "DUE TO TIME LIMIT" in (data[pos + 1]):
                t = float(data[pos + 1].split(': ')[1].split(" ")[0])
            if x['rnd_fp_time'] == 0:
                x['rnd_fp_time'] = t
            else:
                x['hd_fp_time'] = t
        if "Run shortest path substream+timeskip" in line:
            t = 'oot'
            # if len(data) >= pos+1 and not "DUE TO TIME LIMIT" in (data[pos + 1]):
            #     t = float(data[pos + 1].split(': ')[1].split(" ")[0])
            # if x['rnd_sp_time'] == 0:
            #     x['rnd_sp_time'] = t
            # else:
            x['hd_sp_time'] = t

    print(x)
    return x


def getdataforallfilesgreedy():
    df = pd.DataFrame(
        columns=['dataset',
                 'm',
                 'b',
                 'greedy_indexing_time',
                 'greedy_index_size',
                 'ssmin',
                 'ssmed',
                 'ssmax',
                 'ssavg',
                 'ssstdev',
                 'less_than_med',
                 'rnd_ea_time', 'rnd_fp_time', 'rnd_sp_time',
                 'hd_ea_time', 'hd_fp_time', 'hd_sp_time'])

    for ds in datasets:
        files = get_files(ds)

        for f in files:
            x = getdataexp1(f)
            df = df.append(x, ignore_index=True)

    df.to_csv(output_path + 'greedyresults.csv')



if __name__ == '__main__':
    getdataforallfilesgreedy()