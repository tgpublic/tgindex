import pandas as pd

from os import listdir
from os.path import isfile, join

from common import read_file, results_path, datasets, output_path


def get_files_intervals(name):
    files = [f for f in listdir(results_path) if isfile(join(results_path, f))]
    result = []
    for f in files:
        if name in f and ("interval" in f):
            result.append(f)
    return result


def getintervaldata(file):
    data = read_file(results_path + file)
    dataset = file.split("_")[1]
    pos = 0
    result = []
    while pos < len(data):
        line = data[pos]
        if "div" in line:

            x = {
                'dataset': dataset,
                'div': line.split(": ")[1],
                'substream_rnd_ea_time': 0,
                'timeskip_rnd_ea_time': 0,
                'onepass_rnd_ea_time': 0,
                'dl_rnd_ea_time': 0,
                'xuan_rnd_ea_time': 0,
                'substream_rnd_fp_time': 0,
                'timeskip_rnd_fp_time': 0,
                'onepass_rnd_fp_time': 0,
                'label_rnd_fp_time': 0,
            }

            while pos + 1 < len(data) and not "div" in data[pos + 1]:
                line = data[pos]
                if "Run earliest path no index, Adjacency list version, Xuan Paper" in line:
                    after = float(data[pos + 1].split(': ')[1].split("\t")[0])
                    x['xuan_rnd_ea_time'] = after
                elif "Run earliest path substream+timeskip" in line:
                    after = float(data[pos + 1].split(': ')[1].split("\t")[0])
                    x['substream_rnd_ea_time'] = after
                elif "Run fastest path substream+timeskip" in line:
                    after = float(data[pos + 1].split(': ')[1].split("\t")[0])
                    x['substream_rnd_fp_time'] = after
                elif "Run earliest path timeskip only" in line:
                    after = float(data[pos + 2].split(': ')[1].split("\t")[0])
                    x['timeskip_rnd_ea_time'] = after
                elif "Run earliest path no index, Adjacency list version" in line:
                    after = float(data[pos + 1].split(': ')[1].split("\t")[0])
                    x['dl_rnd_ea_time'] = after
                elif "Run earliest path no index" in line:
                    after = float(data[pos + 1].split(': ')[1].split("\t")[0])
                    x['onepass_rnd_ea_time'] = after
                elif "Run fastest path timeskip only" in line:
                    after = float(data[pos + 1].split(': ')[1].split("\t")[0])
                    x['timeskip_rnd_fp_time'] = after
                elif "Run fastest path no index" in line:
                    after = float(data[pos + 1].split(': ')[1].split("\t")[0])
                    x['onepass_rnd_fp_time'] = after
                elif "Run fastest path adj list version" in line:
                    if "TIME" in data[pos + 1]:
                        continue
                    after = float(data[pos + 1].split(': ')[1].split("\t")[0])
                    x['label_rnd_fp_time'] = after
                pos += 1
            result.append(x)
        pos += 1

    print(result)
    return result


def parse_interval_data():
    df = pd.DataFrame(
        columns=['dataset',
                 'div',
                 'substream_rnd_ea_time', 'timeskip_rnd_ea_time', 'onepass_rnd_ea_time', 'dl_rnd_ea_time',
                 'xuan_rnd_ea_time',
                 'substream_rnd_fp_time', 'timeskip_rnd_fp_time', 'onepass_rnd_fp_time', 'label_rnd_fp_time'
                 ])

    for ds in datasets:
        files = get_files_intervals(ds)

        for f in files:
            x = getintervaldata(f)
            for line in x:
                df = df.append(line, ignore_index=True)

    # divone = add_divone()
    # for d in divone:
    #     df = df.append(d, ignore_index=True)

    df.to_csv(output_path + 'intervalresults.csv')


if __name__ == '__main__':
    parse_interval_data()
