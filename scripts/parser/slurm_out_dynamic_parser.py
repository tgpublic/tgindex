import pandas as pd

from os import listdir
from os.path import isfile, join

from common import read_file, results_path, datasets, output_path


def get_files_intervals(name):
    files = [f for f in listdir(results_path) if isfile(join(results_path, f))]
    result = []
    for f in files:
        if name in f and ("dynamic" in f):
            result.append(f)
    return result


def getdynamicdata(file):
    data = read_file(results_path + file)
    name = file.split('_')[1]

    x = {
        'dataset': name,
         'delete_total':0,
         'delete_avg':0,
         'insert_total':0,
         'insert_avg':0
    }
    for pos in range(0, len(data)):
        line = data[pos]
        if "deleted edges in total time: " in line:
            m = line.split("deleted edges in total time: ")[1]
            x['delete_total'] = float(m)
        if "deleted edges on average: " in line:
            m = line.split("deleted edges on average: ")[1]
            x['delete_avg'] = float(m)
        if "added edges in total time: " in line:
            m = line.split("added edges in total time: ")[1]
            x['insert_total'] = float(m)
        if "added edges on average: " in line:
            m = line.split("added edges on average: ")[1]
            x['insert_avg'] = float(m)


    print(x)
    return x



def parse_interval_data():
    df = pd.DataFrame(
        columns=['dataset',
                 'delete_total',
                 'delete_avg',
                 'insert_total',
                 'insert_avg'
                 ])

    for ds in datasets:
        files = get_files_intervals(ds)

        for f in files:
            x = getdynamicdata(f)
            df = df.append(x, ignore_index=True)

    df.to_csv(output_path + 'dynamicresults.csv')


if __name__ == '__main__':
    parse_interval_data()