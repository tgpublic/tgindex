from os import listdir
from os.path import isfile, join

import pandas as pd

from common import read_file, results_path, datasets, output_path


def get_files(name):
    files = [f for f in listdir(results_path) if isfile(join(results_path, f))]
    result = []
    for f in files:
        if name in f and ("exp1" in f or "exp2" in f):
            result.append(f)
    return result


def get_files_baselines(name):
    files = [f for f in listdir(results_path) if isfile(join(results_path, f))]
    result = []
    for f in files:
        if name in f and ("baseline" in f):
            result.append(f)
    return result


def get_times(files, b):
    for f in files:
        if b in f:
            data = read_file(results_path + f)
            print(data)


def getdataexp1(file):
    data = read_file(results_path + file)
    x = {
        'dataset': file.split("_")[2],
        'm': 0,
        'b': 0,
        'h': 0,
        'substream_indexing_time': 0,
        'substream_index_size': 0,
        'ssmin': 0,
        'ssmed': 0,
        'ssmax': 0,
        'ssavg': 0,
        'ssstdev': 0,
        'less_than_med': 0,
        'rnd_ea_time': 0,
        'rnd_fp_time': 0,
        'rnd_sp_time': 0,
        'hd_ea_time': 0,
        'hd_fp_time': 0,
        'hd_sp_time': 0
    }
    for pos in range(0, len(data)):
        line = data[pos]
        if "set memblock_size " in line:
            m = line.split("set memblock_size ")[1]
            x['m'] = int(m)
        if "set num buckets " in line:
            b = line.split("set num buckets ")[1]
            x['b'] = int(b)
        if "set hash_size " in line:
            h = line.split("set hash_size ")[1]
            x['h'] = int(h)
        if "^^computed substream index" in line:
            before = float(data[pos - 1].split(': ')[1])
            after = float(data[pos + 1].split(': ')[1])
            x['substream_indexing_time'] = before + after
        if "min, med, max: " in line:
            vals = line.split("min, med, max: ")[1].split(" ")
            x['ssmin'] = int(vals[0])
            x['ssmed'] = int(vals[1])
            x['ssmax'] = int(vals[2])
        if "avg. bucket size: " in line:
            vals = line.split("avg. bucket size: ")[1].split(" +- ")
            x['ssavg'] = float(vals[0])
            x['ssstdev'] = float(vals[1])
        if "mem. total size: " in line:
            b = line.split("mem. total size: ")[1].split(" ")[0]
            x['substream_index_size'] = float(b)
        if "num vertices in substream with size less than median: " in line:
            b = line.split("num vertices in substream with size less than median: ")[1].split(" ")[0]
            x['less_than_med'] = int(b)
        if "Run earliest path substream+timeskip" in line:
            t = 'oot'
            if not "DUE TO TIME LIMIT" in (data[pos + 1]):
                t = float(data[pos + 1].split(': ')[1].split(" ")[0])
            if x['rnd_ea_time'] == 0:
                x['rnd_ea_time'] = t
            else:
                x['hd_ea_time'] = t
        if "Run fastest path substream+timeskip" in line:
            t = 'oot'
            if not "DUE TO TIME LIMIT" in (data[pos + 1]):
                t = float(data[pos + 1].split(': ')[1].split(" ")[0])
            if x['rnd_fp_time'] == 0:
                x['rnd_fp_time'] = t
            else:
                x['hd_fp_time'] = t
        if "Run shortest path substream+timeskip" in line:
            t = 'oot'
            if not "DUE TO TIME LIMIT" in (data[pos + 1]):
                t = float(data[pos + 1].split(': ')[1].split(" ")[0])
            if x['rnd_sp_time'] == 0:
                x['rnd_sp_time'] = t
            else:
                x['hd_sp_time'] = t

    print(x)
    return x


def getdataforallfilesexp1():
    df = pd.DataFrame(
        columns=['dataset',
                 'm',
                 'b',
                 'h',
                 'substream_indexing_time',
                 'substream_index_size',
                 'ssmin',
                 'ssmed',
                 'ssmax',
                 'ssavg',
                 'ssstdev',
                 'less_than_med',
                 'rnd_ea_time', 'rnd_fp_time', 'rnd_sp_time',
                 'hd_ea_time', 'hd_fp_time', 'hd_sp_time'])

    for ds in datasets:
        files = get_files(ds)

        for f in files:
            x = getdataexp1(f)
            df = df.append(x, ignore_index=True)

    df.to_csv(output_path + 'exp1results.csv')


def getdatabaseline(file):
    data = read_file(results_path + file)
    x = {
        'dataset': file[7:].split("_")[0],
        'timeskip_indexing_time': 0,
        'timeskip_index_size': 0,
        'timeskip_rnd_ea_time': 0,
        'onepass_rnd_ea_time': 0,
        'dl_rnd_ea_time': 0,
        'xuan_rnd_ea_time': 0,
        'timeskip_rnd_fp_time': 0,
        'onepass_rnd_fp_time': 0,
        'label_rnd_fp_time': 0,
        'timeskip_rnd_sp_time': 0,
        'onepass_rnd_sp_time': 0,
        'label_rnd_sp_time': 0,
        'timeskip_hd_ea_time': 0,
        'onepass_hd_ea_time': 0,
        'dl_hd_ea_time': 0,
        'xuan_hd_ea_time': 0,
        'timeskip_hd_fp_time': 0,
        'onepass_hd_fp_time': 0,
        'label_hd_fp_time': 0,
        'timeskip_hd_sp_time': 0,
        'onepass_hd_sp_time': 0,
        'label_hd_sp_time': 0,
    }
    for pos in range(0, len(data)):
        line = data[pos]
        if "Time for computing TSI: " in line:
            m = line.split("Time for computing TSI: ")[1]
            x['timeskip_indexing_time'] = float(m)
        if "Run earliest path timeskip only" in line:
            after = float(data[pos + 2].split(': ')[1].split("\t")[0])
            if x['timeskip_rnd_ea_time'] == 0:
                x['timeskip_rnd_ea_time'] = after
            else:
                x['timeskip_hd_ea_time'] = after
        if "Run earliest path no index, Adjacency list version, Paper Xuan" in line:
            after = float(data[pos + 1].split(': ')[1].split("\t")[0])
            if x['xuan_rnd_ea_time'] == 0:
                x['xuan_rnd_ea_time'] = after
            else:
                x['xuan_hd_ea_time'] = after
        elif "Run earliest path no index, Adjacency list version" in line:
            after = float(data[pos + 1].split(': ')[1].split("\t")[0])
            if x['dl_rnd_ea_time'] == 0:
                x['dl_rnd_ea_time'] = after
            else:
                x['dl_hd_ea_time'] = after
        elif "Run earliest path no index" in line:
            after = float(data[pos + 1].split(': ')[1].split("\t")[0])
            if x['onepass_rnd_ea_time'] == 0:
                x['onepass_rnd_ea_time'] = after
            else:
                x['onepass_hd_ea_time'] = after
        if "Run fastest path timeskip only" in line:
            after = float(data[pos + 2].split(': ')[1].split("\t")[0])
            if x['timeskip_rnd_fp_time'] == 0:
                x['timeskip_rnd_fp_time'] = after
            else:
                x['timeskip_hd_fp_time'] = after
        if "Run fastest path no index" in line:
            after = float(data[pos + 1].split(': ')[1].split("\t")[0])
            if x['onepass_rnd_fp_time'] == 0:
                x['onepass_rnd_fp_time'] = after
            else:
                x['onepass_hd_fp_time'] = after
        if "Run fastest path adj list version" in line:
            if "TIME" in data[pos + 1]:
                continue
            after = float(data[pos + 1].split(': ')[1].split("\t")[0])
            if x['label_rnd_fp_time'] == 0:
                x['label_rnd_fp_time'] = after
            else:
                x['label_hd_fp_time'] = after
        if "Run shortest path timeskip only" in line:
            after = float(data[pos + 2].split(': ')[1].split("\t")[0])
            if x['timeskip_rnd_sp_time'] == 0:
                x['timeskip_rnd_sp_time'] = after
            else:
                x['timeskip_hd_sp_time'] = after
        if "Run shortest path no index" in line:
            after = float(data[pos + 1].split(': ')[1].split("\t")[0])
            if x['onepass_rnd_sp_time'] == 0:
                x['onepass_rnd_sp_time'] = after
            else:
                x['onepass_hd_sp_time'] = after
        if "Run shortest path adj list version" in line:
            after = float(data[pos + 1].split(': ')[1].split("\t")[0])
            if x['label_rnd_sp_time'] == 0:
                x['label_rnd_sp_time'] = after
            else:
                x['label_hd_sp_time'] = after

    print(x)
    return x


def parse_baselines():
    df = pd.DataFrame(
        columns=['dataset',
                 'timeskip_indexing_time',
                 'timeskip_index_size',
                 'timeskip_rnd_ea_time', 'onepass_rnd_ea_time', 'dl_rnd_ea_time', 'xuan_rnd_ea_time',
                 'timeskip_rnd_fp_time', 'onepass_rnd_fp_time', 'label_rnd_fp_time', 'timeskip_rnd_sp_time',
                 'onepass_rnd_sp_time',
                 'label_rnd_sp_time',
                 'timeskip_hd_ea_time', 'onepass_hd_ea_time', 'dl_hd_ea_time', 'xuan_hd_ea_time',
                 'timeskip_hd_fp_time', 'onepass_hd_fp_time', 'label_hd_fp_time',
                 'timeskip_hd_sp_time', 'onepass_hd_sp_time',
                 'label_hd_sp_time'
                 ])

    for ds in datasets:
        files = get_files_baselines(ds)

        for f in files:
            x = getdatabaseline(f)
            df = df.append(x, ignore_index=True)

    df.to_csv(output_path + 'baselineresults.csv')


if __name__ == '__main__':
    getdataforallfilesexp1()
    parse_baselines()
