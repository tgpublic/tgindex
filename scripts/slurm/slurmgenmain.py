cmd_path = './'
dataset_path = '../../datasets/'
slurm_files_path = 'output/'

datasets = {
    "infectious": 10000000,
    "sx-askubuntu-a2q": 10000000,
    "digg": 10000000,
    "prosper": 10000000,
    "arxiv": 10000000,
    "youtube": 2048,
    "sx-stackoverflow-a2q": 2048
}

preemble = '''#!/bin/bash

#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --time=48:00:00
'''


def write_to_file(filename, s):
    f = open(slurm_files_path + filename, 'w')
    f.write(s)
    f.close()


def make_command(dataset, mode, m, b, h):
    cmd = cmd_path + "tgindex " + dataset_path + dataset + ".tg2 " + str(mode) \
          + " -i=6 -m=" + str(m) + " -b=" + str(b) + " -h=" + str(h)
    return cmd


def make_slurm_files():
    filenames = []
    rounds = 10

    # hs = [1,2,4,8,16,32,64]
    bs = [512, 1024, 2048, 4096]

    for ds in datasets.keys():
        filename = 'tgindex' + ds + '_baselines.slurm'
        filenames.append(filename)
        s = preemble
        s += '#SBATCH --output=' + filename + '.out\n\n'
        s += make_command(ds, 4, datasets[ds], 2048, 8) + "\n"
        write_to_file(filename, s)
        for b in bs:
            for r in range(0, rounds):
                filename = 'tgindex_exp1_' + ds + '_b' + str(b) + '_' + str(r) + '.slurm'
                filenames.append(filename)

                s = preemble
                s += '#SBATCH --output=' + filename + '.out\n\n'
                s += make_command(ds, 0, datasets[ds], b, 8) + "\n"
                write_to_file(filename, s)

    return filenames


def make_slurm_files_exp2():
    filenames = []
    rounds = 10

    hs = [1, 2, 4, 16, 32, 64]

    for ds in datasets.keys():
        for h in hs:
            for r in range(0, rounds):
                filename = 'tgindex_exp2_' + ds + '_h' + str(h) + '_' + str(r) + '.slurm'
                filenames.append(filename)

                s = preemble
                s += '#SBATCH --output=' + filename + '.out\n\n'
                s += make_command(ds, 0, datasets[ds], 2048, h) + "\n"
                write_to_file(filename, s)

    return filenames


def make_bash(filenames):
    s = '#!/bin/bash\n\n'
    for f in filenames:
        s += "sbatch " + f + '\n'

    write_to_file('schedule_all_exps.sh', s)


if __name__ == '__main__':
    filenames = make_slurm_files()
    filenames2 = make_slurm_files_exp2()
    all = filenames + filenames2
    make_bash(all)
