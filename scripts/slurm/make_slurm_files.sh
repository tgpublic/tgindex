#!/bin/bash

mkdir output

python3 slurmgenmain.py
python3 slurmgengreedy.py
python3 slurmgencloseness.py
python3 slurmgendynamic.py
python3 slurmgenintervals.py

mv output/* ../../tgindex/cmake-build-release/

chmod +x ../../tgindex/cmake-build-release/schedule_all_exps.sh
chmod +x ../../tgindex/cmake-build-release/schedule_all_greedy.sh
chmod +x ../../tgindex/cmake-build-release/schedule_all_closeness.sh
chmod +x ../../tgindex/cmake-build-release/schedule_all_dynamic.sh
chmod +x ../../tgindex/cmake-build-release/schedule_all_interval.sh

rm -r output
