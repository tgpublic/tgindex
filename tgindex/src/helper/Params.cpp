#include <vector>
#include "Params.h"
#include "SGLog.h"

using namespace std;

void show_help() {
    std::cout
            << "tgindex dataset action [-b=buckets (2048)] [-h=hashsize (8)] [-m=batch size (2048)] [-d=dynamic mode (0)] [-g=greedy (0)]"
            << std::endl;
    std::cout << "\nactions:" << std::endl;
    std::cout << Params::INDEX << "\t compute index and run queries" << std::endl;
    std::cout << Params::BASELINES_ONLY << "\t run queries with baselines only" << std::endl;
    std::cout << Params::TG_STATS << "\t show tg statistics" << std::endl;
    std::cout << "\nQuery (default [-i=0]):" << std::endl;
    std::cout << Params::EA << "\t earliest arrival" << std::endl;
    std::cout << Params::EA_INTERVALS << "\t earliest arrival with intervals" << std::endl;
    std::cout << Params::CLOSENESS << "\t closeness" << std::endl;
    std::cout << Params::FASTEST << "\t fastest paths" << std::endl;
//    std::cout << Params::ONLINE_MODE << "\t load online" << std::endl;
    std::cout << Params::EA_AND_FP << "\t earliest arrival and fastest paths queries" << std::endl;
}

bool Params::parseArgs(vector<string> args) {
    if (args.size() < 2) {
        return false;
    }
    for (auto &arg: args) {
        SGLog::log() << arg << " ";
    }
    SGLog::log() << endl;

    try {
        dataset_path = args[0];
        mode = (MODES) stoi(args[1]);
    } catch (...) {
        return false;
    }

    for (size_t i = 2; i < args.size(); ++i) {
        string next = args[i];
        if (next.length() < 4) return false;
        if (next.substr(0, 3) == "-b=") {
            string valstr = next.substr(3);
            buckets = stoi(valstr);
            SGLog::log() << "set num buckets " << buckets << endl;
        } else if (next.substr(0, 4) == "-hd=") {
            string valstr = next.substr(4);
            highest_degree_queries = stoi(valstr) == 1;
            SGLog::log() << "set highest_degree_queries " << highest_degree_queries << endl;
        } else if (next.substr(0, 3) == "-h=") {
            string valstr = next.substr(3);
            hash_size = stoi(valstr);
            SGLog::log() << "set hash_size " << hash_size << endl;
        } else if (next.substr(0, 3) == "-y=") {
            string valstr = next.substr(3);
            rnd_seed = stoul(valstr);
            SGLog::log() << "set rnd seed " << rnd_seed << endl;
        } else if (next.substr(0, 3) == "-e=") {
            string valstr = next.substr(3);
            use_EM = stoi(valstr) > 0;
            SGLog::log() << "set use_EM " << use_EM << endl;
        } else if (next.substr(0, 3) == "-x=") {
            string valstr = next.substr(3);
            closeness_mode = stoi(valstr);
            SGLog::log() << "set closeness_mode " << closeness_mode << endl;
        } else if (next.substr(0, 3) == "-d=") {
            string valstr = next.substr(3);
            enable_dynamic = stoi(valstr) > 0;
            SGLog::log() << "set enable_dynamic " << enable_dynamic << endl;
        } else if (next.substr(0, 3) == "-g=") {
            string valstr = next.substr(3);
            use_greedy = stoi(valstr) > 0;
            SGLog::log() << "set use_greedy " << use_greedy << endl;
        } else if (next.substr(0, 3) == "-p=") {
            string valstr = next.substr(3);
            print_bucketsize = stoi(valstr) > 0;
            SGLog::log() << "set print_bucketsize " << print_bucketsize << endl;
        } else if (next.substr(0, 4) == "-ps=") {
            string valstr = next.substr(4);
            print_index_infos = stoi(valstr) > 0;
            SGLog::log() << "set print_index_infos " << print_index_infos << endl;
        } else if (next.substr(0, 3) == "-i=") {
            string valstr = next.substr(3);
            index = stoul(valstr);
            SGLog::log() << "set index " << index << endl;
        } else if (next.substr(0, 3) == "-o=") {
            string valstr = next.substr(3);
            saveTGS = stoi(valstr) > 0;
            SGLog::log() << "set saveTGS " << saveTGS << endl;
        } else if (next.substr(0, 3) == "-m=") {
            string valstr = next.substr(3);
            memblock_size = stoul(valstr);
            SGLog::log() << "set memblock_size " << memblock_size << endl;
        } else if (next.substr(0, 3) == "-s=") {
            string valstr = next.substr(3);
            start_pos = stoi(valstr);
            SGLog::log() << "set num start_pos " << start_pos << endl;
        } else if (next.substr(0, 3) == "-u=") {
            string valstr = next.substr(3);
            directed = stoi(valstr) == 0;
            SGLog::log() << "set directed " << directed << endl;
        } else {
            return false;
        }
    }
    return true;
}