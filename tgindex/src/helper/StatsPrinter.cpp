#include "StatsPrinter.h"

using namespace std;

void print_bucket_sizes(Index const &index, Params const &params) {
    ofstream out;
    out.open(params.dataset_path + "_" + to_string(params.buckets) + "_" + to_string(params.hash_size) + "_g" +
             to_string(params.use_greedy) + ".sssizes");
    for (const auto &edgeSet: index.edgeSets) {
        out << edgeSet.size() << endl;
    }
    out.close();
    out.open(params.dataset_path + "_" + to_string(params.buckets) + "_" + to_string(params.hash_size) + "_g" +
             to_string(params.use_greedy) + ".isizes");
    for (unsigned int bucket_vertex_size: index.bucket_vertex_sizes) {
        out << bucket_vertex_size << endl;
    }
    out.close();
}

void print_tg_size(TemporalGraphStream const &tgs) {
    double result = 0;
    result += tgs.num_nodes * sizeof(unsigned int);
    result += tgs.tails.size() * sizeof(NodeId);
    result += tgs.heads.size() * sizeof(NodeId);
    result += tgs.times.size() * sizeof(Time);
    result += tgs.transition_times.size() * sizeof(Time);
    result = result / 1024 / 1024;
    cout << result << " MB" << endl;
    double wc = 0;
    wc += tgs.tails.size() * sizeof(NodeId);
    wc += tgs.heads.size() * sizeof(NodeId);
    wc += tgs.times.size() * sizeof(Time);
    wc += tgs.transition_times.size() * sizeof(Time);
    wc *= 2048;
    wc += tgs.num_nodes * sizeof(unsigned int);
    wc = wc / 1024 / 1024 / 1024;
    cout << "Worst case index size: " << wc << " GB" << endl;
}


void print_index_info(Index const &index, Params const &params, TemporalGraphStream const &tgs) {
    vector<ulong> bucket_sizes;
    for (auto &e: index.edgeSets) {
        bucket_sizes.push_back(e.size());
    }
    sort(bucket_sizes.begin(), bucket_sizes.end());

    print_bucket_sizes(index, params);

    double mean, stdev;
    get_mean_std(bucket_sizes, mean, stdev);

    SGLog::log() << "min, med, max: " << bucket_sizes.front() << " " << bucket_sizes[bucket_sizes.size() / 2] << " "
                 << bucket_sizes.back() << endl;
    SGLog::log() << "avg. bucket size: " << mean << " +- " << stdev << endl;
    auto sizes = getSize(index);
    SGLog::log() << "mem. substream index size: "
                 << (double) index.nodeEdgeSetMapping.size() * sizeof(int) / 1024 / 1024 << " MB" << endl;
    SGLog::log() << "mem. substream size: " << sizes.first << " MB" << endl;
    SGLog::log() << "mem. timeskip size: " << sizes.second << " MB" << endl;
    SGLog::log() << "mem. total size: " << sizes.first + sizes.second << " MB" << endl;

    auto median = bucket_sizes[bucket_sizes.size() / 2];
    unsigned long c = 0;
    for (auto &n: index.nodeEdgeSetMapping) {
        if (index.edgeSets[n].size() <= median) {
            c++;
        }
    }
    SGLog::log() << "num vertices in substream with size less than median: " << c << "\t % "
                 << (double) c / (double) tgs.num_nodes << endl << endl;
}