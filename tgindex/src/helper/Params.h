#ifndef TOPKTRAJ_PARAMS_H
#define TOPKTRAJ_PARAMS_H


#include <string>

void show_help();


struct Params {
    enum MODES {
        INDEX=0,
        BASELINES_ONLY=4,
        TG_STATS=5
    };

    enum QUERY {
        EA=0,
        EA_INTERVALS=1,
        CLOSENESS=2,
        FASTEST=3,
        ONLINE_MODE=5,
        EA_AND_FP=6
    };

    std::string dataset_path;

    MODES mode = INDEX;

    unsigned long buckets = 1000;

    unsigned int hash_size = 8;

    unsigned int rnd_seed = 42;

    unsigned int start_pos = 10;

    unsigned int index = 0;

    bool use_EM = true;
    bool enable_dynamic = false;
    bool use_greedy = false;
    bool print_bucketsize = false;
    bool print_index_infos = false;

    unsigned int memblock_size = 128;

    bool highest_degree_queries = false;

    int closeness_mode = 0;

    bool parseArgs(std::vector<std::string> args);

    bool directed = true;

    bool saveTGS = false;
};

#endif //TOPKTRAJ_PARAMS_H
