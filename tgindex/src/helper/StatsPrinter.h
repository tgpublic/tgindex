#ifndef TPINDEX2_STATSPRINTER_H
#define TPINDEX2_STATSPRINTER_H

#include "../index/EAIndex.h"
#include "SGLog.h"

void print_tg_size(TemporalGraphStream const &tgs);

void print_index_info(Index const &index, Params const &params, TemporalGraphStream const &tgs);

#endif //TPINDEX2_STATSPRINTER_H
