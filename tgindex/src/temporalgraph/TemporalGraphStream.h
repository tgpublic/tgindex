#ifndef TPINDEX2_TEMPORALGRAPHSTREAM_H
#define TPINDEX2_TEMPORALGRAPHSTREAM_H

#include <vector>
#include <string>
#include <limits>
#include <unordered_map>

using NodeId = unsigned int;
using Time = unsigned int;

auto const inf = std::numeric_limits<Time>::max();

struct TemporalGraphStream {
    int num_nodes;
    std::vector<NodeId> heads;
    std::vector<NodeId> tails;
    std::vector<Time> times;
    std::vector<unsigned int> transition_times;
};


TemporalGraphStream loadTGS(const std::string &filename, bool directed);

#endif //TPINDEX2_TEMPORALGRAPHSTREAM_H
