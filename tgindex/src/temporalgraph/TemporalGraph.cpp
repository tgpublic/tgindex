#include "TemporalGraph.h"

void TGNode::addEdge(const TemporalEdge &e) {
    adjlist.push_back(e);
    if (e.t < minTime) minTime = e.t;
    if (e.t > maxTime) maxTime = e.t;
}

TemporalGraph toTemporalGraph(TemporalGraphStream const &tgs) {
    TemporalGraph g;

    for (unsigned int i = 0; i < tgs.num_nodes; ++i) {
        TGNode node;
        node.id = i;
        node.done = false;
        g.nodes.push_back(node);
        g.num_nodes++;
    }

    for (unsigned int i = 0; i < tgs.tails.size(); i++) {
        TemporalEdge e;
        e.u_id = tgs.tails[i];
        e.v_id = tgs.heads[i];
        e.t = tgs.times[i];
        e.id = i;
        e.traversal_time = tgs.transition_times[i];
        g.nodes.at(e.u_id).addEdge(e);
        if (e.t > g.nodes.at(e.u_id).maxTime) g.nodes.at(e.u_id).maxTime = e.t;
        if (e.t < g.nodes.at(e.u_id).minTime) g.nodes.at(e.u_id).minTime = e.t;
    }
    g.num_edges = tgs.tails.size();

    return g;
}
