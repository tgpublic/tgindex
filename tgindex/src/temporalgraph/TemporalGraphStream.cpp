#include <sstream>
#include <iostream>
#include <algorithm>
#include <map>
#include "TemporalGraphStream.h"
#include "../helper/SGLog.h"

using namespace std;

void getFileStream(const string &filename, ifstream &fs) {
    fs.open(filename);
    if (!fs.is_open()) {
        cout << "Could not open data set " << filename << endl;
        exit(EXIT_FAILURE);
    }
}

vector<unsigned long> split_string(string s, char delim) {
    vector<unsigned long> result;
    stringstream ss(s);
    while (ss.good()) {
        string substr;
        getline(ss, substr, delim);
        if (!substr.empty())
            result.push_back(stoul(substr));
    }
    return result;
}

TemporalGraphStream loadTGS(const std::string &filename, bool directed) {
    SGLog::log() << "loading " << filename;
    ifstream fs;
    getFileStream(filename, fs);
    string line;
    getline(fs, line);

    Time mintime = inf;
    TemporalGraphStream tgs;
    tgs.num_nodes = stoi(line);

    struct TEdge {
        NodeId u, v;
        unsigned long t, tt;
    };
    vector<TEdge> edges;

    while (getline(fs, line)) {
        vector<unsigned long> l = split_string(line, ' ');
        NodeId u = l[0];
        NodeId v = l[1];
        unsigned long t = l[2];
        Time tt = l[3];
        edges.push_back({u, v, t, tt});
        if (!directed) {
            edges.push_back({v, u, t, tt});
        }
        if (t < mintime) mintime = t;
    }
    fs.close();

    sort(edges.begin(), edges.end(), [](auto const &e1, auto const &e2) { return e1.t < e2.t; });
    for (auto &e: edges) {
        tgs.tails.push_back(e.u);
        tgs.heads.push_back(e.v);
        tgs.times.push_back(e.t);
        tgs.transition_times.push_back(e.tt);
    }

    SGLog::log() << "...done" << endl;
    SGLog::log() << "#nodes: " << tgs.num_nodes << endl;
    SGLog::log() << "#edges: " << tgs.tails.size() << endl;

    return tgs;
}
