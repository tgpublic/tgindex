#ifndef TGKERNEL_GRAPHDATALOADER_H
#define TGKERNEL_GRAPHDATALOADER_H

#include <string>
#include <utility>
#include <vector>
#include <iostream>
#include <algorithm>
#include <limits>
#include <unordered_set>
#include <unordered_map>
#include <memory>
#include "TemporalGraphStream.h"

using EdgeId = NodeId;

struct TemporalEdge;
struct TGNode;
using AdjacenceList = std::vector<TemporalEdge>;

struct TGNode {
    TGNode() = default;
    explicit TGNode(NodeId nid) : id(nid){};
    NodeId id = 0;
    AdjacenceList adjlist;
    Time minTime = inf;
    Time maxTime = 0;
    bool done = false;
    void addEdge(const TemporalEdge& t);
};


struct TemporalEdge {
    TemporalEdge() = default;

    TemporalEdge(NodeId u, NodeId v, Time time, Time tt, EdgeId eid) : u_id(u), v_id(v), t(time),
                                                                       traversal_time(tt), id(eid), next(nullptr){  }

    TemporalEdge(NodeId u, NodeId v, Time time, Time tt) : u_id(u), v_id(v), t(time),
                                                           traversal_time(tt), id(0), next(nullptr){  }

    NodeId u_id{}, v_id{};
    Time t{};
    Time traversal_time{};

    bool deleted = false;

    bool operator () ( TemporalEdge const * lhs, TemporalEdge const * rhs ) const {
        return lhs->u_id == rhs->u_id && lhs->v_id == rhs->v_id && lhs->t == rhs->t;
    }

    EdgeId id{};

    TemporalEdge *next = nullptr;

    TemporalEdge(const TemporalEdge& e) : u_id(e.u_id), v_id(e.v_id), t(e.t),
                                          traversal_time(e.traversal_time), id(e.id), next(e.next) { }
};


using TGNodes = std::vector<TGNode>;


struct TemporalGraph {
    TemporalGraph () : num_nodes(0), num_edges(0), nodes(TGNodes()), minTimeDelta(0) {};

    TemporalGraph (const TemporalGraph& tg) {
        for (TGNode const &n : tg.nodes) {
            TGNode nn;
            nn.minTime = n.minTime;
            nn.maxTime = n.maxTime;
            nn.id = n.id;
            nn.done = n.done;
            for (auto &e : n.adjlist) {
                nn.adjlist.push_back(e);
            }
            nodes.push_back(nn);
        }

        num_edges = tg.num_edges;
        num_nodes = tg.num_nodes;
        minTimeDelta = tg.minTimeDelta;
    }

    TGNodes nodes;
    unsigned int num_nodes;
    unsigned int num_edges;
    Time minTimeDelta;
};

TemporalGraph toTemporalGraph(TemporalGraphStream const &tgs);

struct label {
    NodeId nid = inf;
    NodeId pid = inf;
    Time s = 0, a = 0, d = 0;
    size_t pq_pos = 0;
    bool deleted = false;
};

class LabelPQASP {

public:

    void push(std::shared_ptr<label> &t) {
        data.push_back(t);
        t->pq_pos = data.size() - 1;
        heapifyUp(data.size() - 1);
    }

    std::shared_ptr<label>& top() {
        return data[0];
    }

    bool empty() {
        return data.empty();
    }

    size_t size() {
        return data.size();
    }

    void pop() {
        data[0] = data.back();
        data[0]->pq_pos = 0;
        data.pop_back();
        heapifyDown();
    }

    void decreasedKey(size_t pos) {
        heapifyUp(pos);
    }

private:

    std::vector<std::shared_ptr<label>> data;

    void heapifyUp(size_t p) {
        size_t child = p;
        size_t parent = getParent(child);

        while (child >= 0 && parent >= 0 && data[child]->a < data[parent]->a) {
            swap(child, parent);
            child = parent;
            parent = getParent(child);
        }
    }

    void heapifyDown(size_t p = 0) {
        size_t length = data.size();
        while (true) {
            size_t left = getLeftChild(p);
            size_t right = getRightChild(p);
            size_t smallest = p;

            if (left < length && data[left]->a < data[smallest]->a)
                smallest = left;

            if (right < length && data[right]->a < data[smallest]->a)
                smallest = right;

            if (smallest != p) {
                swap(smallest, p);
                p = smallest;
            } else break;
        }
    }

    static size_t getParent(size_t child) {
        if (child == 0) return 0;
        if (child % 2 == 0)
            return (child / 2) - 1;
        else
            return child / 2;
    }

    static size_t getLeftChild(size_t parent){
        return 2 * parent + 1;
    }

    static size_t getRightChild(size_t parent){
        return 2 * parent + 2;
    }

    void swap(size_t a, size_t b) {
        data[a]->pq_pos = b;
        data[b]->pq_pos = a;
        data[a].swap(data[b]);
    }
};

template<class T>
class LabelX {

public:

    void push(std::shared_ptr<T> &t) {
        data.push_back(t);
        t->pq_pos = data.size() - 1;
        heapifyUp(data.size() - 1);
    }

    std::shared_ptr<T>& top() {
        return data[0];
    }

    bool empty() {
        return data.empty();
    }

    size_t size() {
        return data.size();
    }

    void pop() {
        data[0] = data.back();
        data[0]->pq_pos = 0;
        data.pop_back();
        heapifyDown();
    }

    void decreasedKey(size_t pos) {
        heapifyUp(pos);
    }

private:

    std::vector<std::shared_ptr<T>> data;

    void heapifyUp(size_t p) {
        size_t child = p;
        size_t parent = getParent(child);

        while (child >= 0 && parent >= 0 && data[child]->a < data[parent]->a) {
            swap(child, parent);
            child = parent;
            parent = getParent(child);
        }
    }

    void heapifyDown(size_t p = 0) {
        size_t length = data.size();
        while (true) {
            size_t left = getLeftChild(p);
            size_t right = getRightChild(p);
            size_t smallest = p;

            if (left < length && data[left]->a < data[smallest]->a)
                smallest = left;

            if (right < length && data[right]->a < data[smallest]->a)
                smallest = right;

            if (smallest != p) {
                swap(smallest, p);
                p = smallest;
            } else break;
        }
    }

    static size_t getParent(size_t child) {
        if (child == 0) return 0;
        if (child % 2 == 0)
            return (child / 2) - 1;
        else
            return child / 2;
    }

    static size_t getLeftChild(size_t parent){
        return 2 * parent + 1;
    }

    static size_t getRightChild(size_t parent){
        return 2 * parent + 2;
    }

    void swap(size_t a, size_t b) {
        data[a]->pq_pos = b;
        data[b]->pq_pos = a;
        data[a].swap(data[b]);
    }
};
#endif //TGKERNEL_GRAPHDATALOADER_H
