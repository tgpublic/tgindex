#ifndef TPINDEX2_EAINDEXGREEDY_H
#define TPINDEX2_EAINDEXGREEDY_H

#include "EAIndex.h"

Index construct_index_greedy_batchwise(TemporalGraphStream &tgs, Params const &params);

#endif //TPINDEX2_EAINDEXGREEDY_H
