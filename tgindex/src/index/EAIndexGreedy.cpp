#include <random>
#include "EAIndexGreedy.h"
#include "../helper/SGLog.h"
#include "EdgeSet.h"

using namespace std;


std::vector<unsigned int> getAllPossibleEdges(TemporalGraphStream const &tgs, NodeId u, unsigned int start_pos) {
    vector<Time> arr(tgs.num_nodes, inf);
    arr[u] = 0;
    vector<unsigned int> result;
    for (unsigned int i = start_pos; i < tgs.tails.size(); ++i) {
        if (arr[tgs.tails[i]] > tgs.times[i]) continue;
        if (arr[tgs.heads[i]] > tgs.times[i] + tgs.transition_times[i])
            arr[tgs.heads[i]] = tgs.times[i] + tgs.transition_times[i];
        result.push_back(i);
    }
    return result;
}


Index construct_index_greedy_batchwise(TemporalGraphStream &tgs, Params const &params) {
    SGLog::log() << "constructing index greedy" << endl;

    Timer timer;
    timer.start();

    auto k = params.buckets;
    auto block_size = params.memblock_size;

    vector<vector<unsigned int>> edges_sets(tgs.num_nodes);
    vector<vector<unsigned int>> eaindex(k);
    vector<unsigned int> nodeEdgeSetMapping(tgs.num_nodes);
    vector<unsigned int> I(k, 0);
    vector<unsigned int> evsizes(tgs.num_nodes, 0);

    vector<unsigned long> start_pos(tgs.num_nodes, inf);
    for (unsigned int i = 0; i < tgs.tails.size(); ++i) {
        if (start_pos[tgs.tails[i]] == inf) {
            start_pos[tgs.tails[i]] = i;
        }
    }

    vector<bool> done(tgs.num_nodes, false);

    vector<vector<NodeId>> nids_s(k, vector<NodeId>());
    vector<NodeId> rnids(tgs.num_nodes);
    std::iota(rnids.begin(), rnids.end(), 0);
    std::shuffle(rnids.begin(), rnids.end(), std::mt19937{42});

    auto num_nodes = tgs.num_nodes;
    uint num_blocks = num_nodes / block_size;
    vector<NodeId> nidmap(tgs.num_nodes, 0);

    for (unsigned int b = 0; b <= num_blocks; ++b) {
        NodeId nidstart = b * block_size;
        NodeId nidend = ((b + 1) * block_size) >= num_nodes ? num_nodes : ((b + 1) * block_size);
        vector<vector<unsigned int>> ues(nidend - nidstart);

#pragma omp parallel for default(none) shared(evsizes, rnids, nidmap, b, nidend, nidstart, tgs, nodeEdgeSetMapping, start_pos, k, done, ues)
        for (unsigned int n = nidstart; n < nidend; ++n) {
            auto nid = rnids[n];
            auto used_edges = getAllPossibleEdges(tgs, nid, start_pos[nid]);
            if (used_edges.empty()) {
                nodeEdgeSetMapping[nid] = k;
                done[nid] = true;
                continue;
            }
            ues[n % (nidend - nidstart)] = used_edges;
            nidmap[nid] = n % (nidend - nidstart);
        }

        for (unsigned int n = nidstart; n < nidend; ++n) {
            auto nid = rnids[n];
            if (done[nid] || nodeEdgeSetMapping[nid] == k) continue;
            Compare2 compare{inf, 0, vector<uint>()};
#pragma omp parallel for reduction(minimum:compare) default(none) shared(nidmap, num_nodes, nids_s, eaindex, ues, I, k, nodeEdgeSetMapping, nid)
            for (int i = 0; i < k; ++i) {
                auto pv = join(eaindex[i], ues[nidmap[nid]]);
                auto jh = I[i] == 0 ? 0 : pv.size();

                if (jh < compare.val) {
                    compare.val = jh;
                    compare.data = pv;
                    compare.index = i;
                }
            }
            nodeEdgeSetMapping[nid] = compare.index;
            eaindex[compare.index] = compare.data;
            I[compare.index]++;
        }
    }

    timer.stopAndPrintTime();
    SGLog::log() << "^^computed substream index" << endl;

    Index index;
    index.edgeSets = std::move(eaindex);
    index.nodeEdgeSetMapping = std::move(nodeEdgeSetMapping);

    index.bucket_vertex_sizes = I;

    ofstream out;
    out.open(params.dataset_path + ".greedyevsizes");
    for (auto &ue: evsizes) {
        out << ue << endl;
    }
    out.close();

    return index;
}
