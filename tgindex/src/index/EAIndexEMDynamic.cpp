#include "EAIndexEM.h"
#include "EAIndexEMDynamic.h"

using namespace std;

DynamicIndex toDynamicIndex(TemporalGraphStream const &tgs, Index &index) {
    DynamicIndex dindex;
    for (EdgeId eid = 0; eid < tgs.tails.size(); ++eid) {
        NodeId u = tgs.tails[eid];
        NodeId v = tgs.heads[eid];
        NodeId t = tgs.times[eid];
        NodeId tt = tgs.transition_times[eid];
        dindex.edges.push_back(TE{u,v,t,tt,eid});
        dindex.edgePositionMap[eid] = dindex.edges.size()-1;
    }
    dindex.edgeSets.resize(index.edgeSets.size());

    for (uint i = 0; i < index.edgeSets.size(); ++i) {
        for (auto &e : index.edgeSets[i]) {
            dindex.edgeSets[i].push_back(e);
        }
    }
    index.edgeSets.clear();

    dindex.nodeEdgeSetMapping = index.nodeEdgeSetMapping;
    index.nodeEdgeSetMapping.clear();


    dindex.edge_contained_simple.resize(dindex.edges.size());
    dindex.vertex_contained_simple.resize(tgs.num_nodes, unordered_set<uint>());

    dindex.vertex_contained_in.resize(tgs.num_nodes,vector<pair<unsigned int, Time>>(
            dindex.edgeSets.size(), pair<unsigned int, Time>(0, inf)));

    for (unsigned int i = 0; i < dindex.edgeSets.size(); ++i) {
        for (unsigned int j = 0; j < dindex.edgeSets[i].size(); ++j) {
            EdgeId eid = dindex.edgeSets[i][j];
            dindex.edge_contained_simple[eid].push_back(i);

            auto te = dindex.getEdge(eid);
            dindex.vertex_contained_simple[te.v].insert(i);

            if (dindex.vertex_contained_in[te.v][i].second > te.t + te.tt) {
                dindex.vertex_contained_in[te.v][i] = {i, te.t + te.tt};
            }
        }
    }


    return dindex;
}

void loadQueryVertex(DynamicIndex &dindex, NodeId nid) {
    auto i = dindex.nodeEdgeSetMapping[nid];
    if (i == dindex.edgeSets.size()) return;

    // reset and load edges
    dindex.clear();

    dindex.timeSkipIndex.resize(dindex.nodeEdgeSetMapping.size(), inf);
    for (unsigned int j = 0; j < dindex.edgeSets[i].size(); ++j) {
        auto &e = dindex.edges[dindex.edgePositionMap[dindex.edgeSets[i][j]]];
        dindex.tailsSets[i].push_back(e.u);
        dindex.headsSets[i].push_back(e.v);
        dindex.timesSets[i].push_back(e.t);
        dindex.transitiontimesSets[i].push_back(e.tt);

        if (dindex.nodeEdgeSetMapping[e.u] == i) {
            if (dindex.timeSkipIndex[e.u] == inf)
                dindex.timeSkipIndex[e.u] = j;
        }
    }

}


void delete_edges(DynamicIndex &dindex, std::vector<EdgeId> const &eids){
    unordered_set<unsigned int> modified_substream;
    set<uint> deleted;
    for (auto eid : eids) {
        deleted.insert(eid);
        for (auto &s : dindex.edge_contained_simple[eid]) {
            modified_substream.insert(s);
        }
    }
    for (auto i : modified_substream) {
        std::vector<uint> newss;
        for (unsigned int eid : dindex.edgeSets[i]) {
            if (deleted.find(eid) != deleted.end()) {
                continue;
            }
            newss.push_back(eid);
        }
        dindex.edgeSets[i] = newss;
    }
}


void insert_edge(TemporalGraphStream const &tgs, DynamicIndex &index, TE &edge) {
    index.edges.push_back(edge);
    index.edgePositionMap[edge.eid] = index.edges.size() - 1;

    // find reachable edges from v at t+tt
    auto v = edge.v;
    auto u = edge.u;
    auto t = edge.t;
    auto tt = edge.tt;
    auto eid = edge.eid;

    vector<Time> arr(tgs.num_nodes, inf);
    arr[v] = t+tt;
    vector<uint> reachable_edges;
    reachable_edges.push_back(eid);

    auto c = index.nodeEdgeSetMapping[v];
    if (c != index.edgeSets.size()) {
        for (auto const &eid1 : index.edgeSets[c]){
            auto e = index.getEdge(eid1);
            if (arr[e.u] > e.t) continue;
            if (arr[e.v] > e.t + e.tt) arr[e.v] = e.t + e.tt;
            reachable_edges.push_back(e.eid);
        }
    }

    for (auto &k : index.vertex_contained_in[u]) {
        if (k.second > t) continue;

        for (auto &eid1 : reachable_edges) {
            auto e = index.getEdge(eid1);
            if (index.vertex_contained_in[e.u][k.first].second > e.t) {
                index.vertex_contained_in[e.u][k.first] = {k.first, e.t};
            }
            if (index.vertex_contained_in[e.v][k.first].second > e.t+e.tt) {
                index.vertex_contained_in[e.v][k.first] = {k.first, e.t+e.tt};
            }
        }

        vector<uint> temp;
        std::merge(index.edgeSets[k.first].begin(), index.edgeSets[k.first].end(),
                   reachable_edges.begin(), reachable_edges.end(), std::back_inserter(temp),
                   [&index](const auto &eid1, const auto &eid2) { return index.getEdge(eid1).t < index.getEdge(eid2).t; });
        index.edgeSets[k.first] = temp;
    }
    if (index.nodeEdgeSetMapping[u] == index.edgeSets.size()) {
        if (index.nodeEdgeSetMapping[v] != index.edgeSets.size())
            index.nodeEdgeSetMapping[u] = index.nodeEdgeSetMapping[v];
        else
            index.nodeEdgeSetMapping[u] = rand() % index.edgeSets.size();
    }
    vector<uint> temp;
    std::merge(index.edgeSets[index.nodeEdgeSetMapping[u]].begin(),
               index.edgeSets[index.nodeEdgeSetMapping[u]].end(),
               reachable_edges.begin(), reachable_edges.end(), std::back_inserter(temp),
               [&index](const auto &a, const auto &b) {  return index.getEdge(a).t < index.getEdge(b).t; });
    index.edgeSets[index.nodeEdgeSetMapping[u]] = temp;

}
