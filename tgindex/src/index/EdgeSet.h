#ifndef TPINDEX2_EDGESET_H
#define TPINDEX2_EDGESET_H

#include <vector>

std::vector<unsigned int> join(const std::vector<unsigned int> &a, const std::vector<unsigned int> &b);

std::pair<double, std::vector<unsigned int>>
minhashJoin4(std::vector<unsigned int> const &a, std::vector<unsigned int> const &b, unsigned int hash_size);

#endif //TPINDEX2_EDGESET_H
