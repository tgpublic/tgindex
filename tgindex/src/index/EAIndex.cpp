#include "EAIndex.h"
#include "EdgeSet.h"
#include <algorithm>
#include <numeric>
#include <random>
#include <list>

using namespace std;

Compare cmpmin(Compare const &a, Compare const &b) {
    if (a.val == b.val) return a.index < b.index ? a : b;
    return a.val < b.val ? a : b;
}

Compare2 cmpmin2(Compare2 const &a, Compare2 const &b) {
    if (a.val == b.val) return a.index < b.index ? a : b;
    return a.val < b.val ? a : b;
}

vector<NodeId> perm;

void initPerm(unsigned long num_nodes) {
    perm.resize(num_nodes);
    std::iota(perm.begin(), perm.end(), 0);
    std::shuffle(perm.begin(), perm.end(), std::mt19937{std::random_device{}()});
}

unsigned int hashNodeId(NodeId a) {
    return perm[a];
}

pair<std::vector<unsigned int>,std::vector<unsigned int>> getAllPossibleEdgesWithMinHash(TemporalGraphStream &tgs, NodeId u, unsigned int start_pos, unsigned int k) {
    vector<Time> arr(tgs.num_nodes, inf);
    vector<unsigned int> hashlist;
    arr[u] = 0;
    vector<unsigned int> result;
    for (unsigned int i = start_pos; i < tgs.tails.size(); ++i) {
        if (arr[tgs.tails[i]] > tgs.times[i]) continue;
        if (arr[tgs.heads[i]] > tgs.times[i] + tgs.transition_times[i])
            arr[tgs.heads[i]] = tgs.times[i] + tgs.transition_times[i];
        result.push_back(i);
        hashlist.push_back(hashNodeId(i));
        sort(hashlist.begin(), hashlist.end());
        if (hashlist.size() > k) hashlist.pop_back();
    }
    return {result, hashlist};
}


void computeStartPositions(TemporalGraphStream const &tgs, Index &index, unsigned int &numStartPositions) {
    index.timeSkipIndex.resize(index.nodeEdgeSetMapping.size(), inf);
    for (unsigned int es = 0; es < index.edgeSets.size(); ++es) {
        for (int j = 0; j < index.edgeSets[es].size(); ++j) {
            auto nid = tgs.tails[index.edgeSets[es][j]];
            if (index.nodeEdgeSetMapping[nid] != es) continue;
            if (index.timeSkipIndex[nid] == inf)
                index.timeSkipIndex[nid] = j;
        }
    }
}


pair<double, double> getSize(Index const &index) {
    auto result = (double)index.nodeEdgeSetMapping.size();
    for (auto &e : index.edgeSets)
        result += (double)e.size();
    double start_pos_size = 0;
    start_pos_size = (double)index.timeSkipIndex.size();
    result = result * sizeof(int)/1024/1024;
    start_pos_size = start_pos_size * sizeof(int)/1024/1024;
    return {result, start_pos_size};
}


