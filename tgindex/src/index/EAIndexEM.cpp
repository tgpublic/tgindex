#include <random>
#include "EAIndexEM.h"
#include "../helper/SGLog.h"
#include "EdgeSet.h"
#include <list>
#include <map>

using namespace std;

#pragma omp declare reduction(vec_uint_plus : std::vector<uint> : \
                              std::transform(omp_out.begin(), omp_out.end(), omp_in.begin(), omp_out.begin(), std::plus<>())) \
                    initializer(omp_priv = omp_orig)

struct my_min {
    template<class T>
    const T &operator()(const T &a, const T &b) const {
        return std::min(a, b);
    }
};

#pragma omp declare reduction(vec_uint_min : std::vector<Time> : \
                              std::transform(omp_out.begin(), omp_out.end(), omp_in.begin(), omp_out.begin(), my_min())) \
                    initializer(omp_priv = omp_orig)


Index construct_index_EM2(TemporalGraphStream &tgs, const Params &params) {
    SGLog::log() << "constructing index" << endl;
    unsigned int k = params.buckets;
    unsigned int numStartPositions = params.start_pos;
    unsigned int hash_size = params.hash_size;
    unsigned int block_size = params.memblock_size;
    vector<unsigned int> evsizes(tgs.num_nodes, 0);

    vector<NodeId> rnids(tgs.num_nodes);
    std::iota(rnids.begin(), rnids.end(), 0);
    std::shuffle(rnids.begin(), rnids.end(), std::mt19937{42});

    SGLog::log() << "num vertices / block_size = " << tgs.num_nodes / block_size << endl;

    initPerm(tgs.tails.size());

    Timer timer;
    timer.start();

    vector<vector<unsigned int>> edges_sets(tgs.num_nodes);
    vector<vector<unsigned int>> eaindexHashes(k);
    vector<unsigned int> nodeEdgeSetMapping(tgs.num_nodes);
    vector<unsigned int> I(k, 0);

    vector<unsigned int> out_edges(tgs.num_nodes, 0);
    vector<Time> start_pos(tgs.num_nodes, inf);
    my_min mm;
#pragma omp parallel for default(none) shared(mm, inf, tgs)\
    reduction(vec_uint_min:start_pos) reduction(vec_uint_plus:out_edges)

    for (unsigned int i = 0; i < tgs.tails.size(); ++i) {
        start_pos[tgs.tails[i]] = mm(start_pos[tgs.tails[i]], i);
        out_edges[tgs.tails[i]]++;
    }

    vector<bool> outdegzero(tgs.num_nodes, false);
#pragma omp parallel for default(none) shared(outdegzero, tgs, out_edges)
    for (unsigned int i = 0; i < tgs.num_nodes; ++i) {
        outdegzero[i] = out_edges[i] == 0;
    }

    Timer t;
    vector<bool> done(tgs.num_nodes, false);

    vector<unsigned long> sizes;
    vector<vector<unsigned int>> hashes(tgs.num_nodes);
    vector<NodeId> nidmap(tgs.num_nodes, 0);

    vector<vector<unsigned int>> eaindex(k);

    auto num_nodes = tgs.num_nodes;
    uint num_blocks = num_nodes / block_size;

    vector<double> times_reachability;
    Timer timer2;

    for (unsigned int b = 0; b <= num_blocks; ++b) {
        NodeId nidstart = b * block_size;
        NodeId nidend = ((b + 1) * block_size) >= num_nodes ? num_nodes : ((b + 1) * block_size);
        vector<vector<unsigned int>> ues(nidend - nidstart);

#pragma omp parallel for default(none) shared(outdegzero, evsizes, rnids, nidmap, hashes, b, nidend, nidstart, tgs, nodeEdgeSetMapping, start_pos, hash_size, k, done, ues)
        for (unsigned int n = nidstart; n < nidend; ++n) {
            auto nid = rnids[n];
            if (outdegzero[nid]) {
                nodeEdgeSetMapping[nid] = k;
                done[nid] = true;
                continue;
            }
            auto used_edges = getAllPossibleEdgesWithMinHash(tgs, nid, start_pos[nid], hash_size);
            ues[n % (nidend - nidstart)] = used_edges.first;
            nidmap[nid] = n % (nidend - nidstart);
            hashes[nid] = used_edges.second;
            evsizes[nid] = used_edges.first.size();
        }

        vector<vector<NodeId>> nids_s(k, vector<NodeId>());

        for (unsigned int n = nidstart; n < nidend; ++n) {
            auto nid = rnids[n];
            if (done[nid] || nodeEdgeSetMapping[nid] == k) continue;
            Compare compare{(double) inf, 0};
#pragma omp parallel for reduction(minimum:compare) default(none) shared(num_nodes, nids_s, hashes, hash_size, I, k, nodeEdgeSetMapping, nid, eaindexHashes)
            for (int j = 0; j < k; ++j) {
                auto pv = minhashJoin4(eaindexHashes[j], hashes[nid], hash_size);
                double jh = (I[j] + 1.0) * (1.0 + pv.first);

                if (jh < compare.val) {
                    compare.val = jh;
                    compare.index = j;
                    compare.hash = pv.second;
                }
            }

            eaindexHashes[compare.index] = compare.hash;
            nodeEdgeSetMapping[nid] = compare.index;
            I[compare.index]++;
            nids_s[compare.index].push_back(nid);
        }

#pragma omp parallel for default(none) shared(k, nids_s, nidmap, nodeEdgeSetMapping, eaindex, ues)
        for (int i = 0; i < k; ++i) {
            for (int j = 0; j < nids_s[i].size(); ++j) {
                eaindex[i] = join(eaindex[i], ues[nidmap[nids_s[i][j]]]);
            }
        }
    }

    timer.stopAndPrintTime();
    SGLog::log() << "^^computed substream index" << endl;

    Index index;
    index.edgeSets = std::move(eaindex);
    index.nodeEdgeSetMapping = std::move(nodeEdgeSetMapping);

    index.bucket_vertex_sizes = I;

    timer.start();
    computeStartPositions(tgs, index, numStartPositions);
    timer.stopAndPrintTime();
    SGLog::log() << "^^computed timeskip index" << endl;

    ofstream out;
    out.open(params.dataset_path + ".evsizes");
    for (auto &ue: evsizes) {
        out << ue << endl;
    }
    out.close();

    return index;
}


void loadQueryVertex(TemporalGraphStream const &tgs, Index &index, NodeId nid) {
    auto i = index.nodeEdgeSetMapping[nid];
    if (i == index.edgeSets.size()) return;

    // reset and load edges
    index.headsSets.clear();
    index.tailsSets.clear();
    index.timesSets.clear();
    index.transitiontimesSets.clear();
    for (unsigned int j = 0; j < index.edgeSets.size(); ++j) {
        index.headsSets.emplace_back();
        index.tailsSets.emplace_back();
        index.timesSets.emplace_back();
        index.transitiontimesSets.emplace_back();
    }

    for (unsigned int j = 0; j < index.edgeSets[i].size(); ++j) {
        index.tailsSets[i].push_back(tgs.tails[index.edgeSets[i][j]]);
        index.headsSets[i].push_back(tgs.heads[index.edgeSets[i][j]]);
        index.timesSets[i].push_back(tgs.times[index.edgeSets[i][j]]);
        index.transitiontimesSets[i].push_back(tgs.transition_times[index.edgeSets[i][j]]);
    }

    //load timeskip index
    index.timeSkipIndex.resize(index.nodeEdgeSetMapping.size(), inf);
    for (int j = 0; j < index.edgeSets[i].size(); ++j) {
        auto n = tgs.tails[index.edgeSets[i][j]];
        if (index.nodeEdgeSetMapping[n] != i) continue;
        if (index.timeSkipIndex[n] == inf)
            index.timeSkipIndex[n] = j;
    }
}
