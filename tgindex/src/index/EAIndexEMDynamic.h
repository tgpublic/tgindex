#ifndef TPINDEX2_EAINDEXEMDYNAMIC_H
#define TPINDEX2_EAINDEXEMDYNAMIC_H

#include "EAIndex.h"


struct DynamicIndex {
    std::vector<TE> edges;
    std::map<uint, EdgeId> edgePositionMap;

    std::vector<std::vector<unsigned int>> edgeSets;
    std::vector<unsigned int> nodeEdgeSetMapping;
    std::vector<unsigned int> bucket_vertex_sizes;

    // for dynamic updates
    std::vector<std::vector<unsigned int>> edge_contained_simple;
    std::vector<std::unordered_set<uint>> vertex_contained_simple;
    std::vector<std::vector<std::pair<unsigned int, Time>>> vertex_contained_in;


    std::vector<std::vector<unsigned int>> tailsSets;
    std::vector<std::vector<unsigned int>> headsSets;
    std::vector<std::vector<unsigned int>> timesSets;
    std::vector<std::vector<unsigned int>> transitiontimesSets;

    TimeSkipIndex timeSkipIndex;

    [[nodiscard]] unsigned long getStartPosition(NodeId nid) const {
        return timeSkipIndex[nid];
    }

    [[nodiscard]] TE getEdge(EdgeId eid) {
        return edges[edgePositionMap[eid]];
    }

    void clear() {
        headsSets.clear();
        tailsSets.clear();
        timesSets.clear();
        transitiontimesSets.clear();
        headsSets.resize(edgeSets.size());
        tailsSets.resize(edgeSets.size());
        timesSets.resize(edgeSets.size());
        transitiontimesSets.resize(edgeSets.size());
    }
};

void loadQueryVertex(DynamicIndex &dindex, NodeId nid);

DynamicIndex toDynamicIndex(TemporalGraphStream const &tgs, Index &index);

void delete_edges(DynamicIndex &index, std::vector<EdgeId> const &eids);

void insert_edge(TemporalGraphStream const &tgs, DynamicIndex &index, TE &edge);

#endif //TPINDEX2_EAINDEXEMDYNAMIC_H
