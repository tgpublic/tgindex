#ifndef TPINDEX2_EAINDEXEM_H
#define TPINDEX2_EAINDEXEM_H

#include "EAIndex.h"

Index construct_index_EM2(TemporalGraphStream &tgs, const Params &params);
void loadQueryVertex(TemporalGraphStream const &tgs, Index &index, NodeId nid);

#endif //TPINDEX2_EAINDEXEM_H
