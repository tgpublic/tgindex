#include <algorithm>
#include "EdgeSet.h"

using namespace std;

vector<unsigned int>
intersect2(const vector<unsigned int> &a, const vector<unsigned int> &b, const vector<unsigned int> &c) {
    vector<unsigned int> result;
    vector<unsigned int> ab;
    std::set_intersection(a.begin(), a.end(),
                          b.begin(), b.end(),
                          std::back_inserter(ab));
    std::set_intersection(ab.begin(), ab.end(),
                          c.begin(), c.end(),
                          std::back_inserter(result));
    return result;
}

vector<unsigned int> join(const vector<unsigned int> &a, const vector<unsigned int> &b) {
    vector<unsigned int> result;
    std::set_union(a.begin(), a.end(),
                   b.begin(), b.end(),
                   std::back_inserter(result));

    result.erase(unique(result.begin(), result.end()), result.end());
    return result;
}

pair<double, vector<unsigned int>>
minhashJoin4(vector<unsigned int> const &a, vector<unsigned int> const &b, unsigned int hash_size) {
    vector<unsigned int> vec(2 * hash_size);
    auto it = set_union(a.begin(), a.end(), b.begin(), b.end(), vec.begin());
    vec.resize(min((unsigned int) (it - vec.begin()), hash_size));
    auto sb = intersect2(a, b, vec);
    auto jaccard = 1.0 - ((double) sb.size() / ((double) hash_size));
    return {jaccard, vec};
}
