#ifndef TPINDEX2_EAINDEX_H
#define TPINDEX2_EAINDEX_H

#include "../temporalgraph/TemporalGraphStream.h"
#include "../helper/Params.h"
#include "../temporalgraph/TemporalGraph.h"
#include <set>
#include <map>

struct Compare { double val{}; unsigned int index{}; std::vector<unsigned int> hash; };
Compare cmpmin(Compare const &a, Compare const &b);

#pragma omp declare reduction(minimum : Compare : omp_out = cmpmin(omp_out, omp_in)) \
        initializer(omp_priv=omp_orig)

struct Compare2 { uint val{}; ; unsigned int index{}; std::vector<unsigned int> data{}; };
Compare2 cmpmin2(Compare2 const &a, Compare2 const &b);

#pragma omp declare reduction(minimum : Compare2 : omp_out = cmpmin2(omp_out, omp_in)) \
        initializer(omp_priv=omp_orig)


using TimeSkipIndex = std::vector<Time>;

struct TE{
    NodeId u,v;
    unsigned long t,tt;
    EdgeId eid;
};

struct Index {
    std::vector<std::vector<unsigned int>> edgeSets;
    std::vector<unsigned int> nodeEdgeSetMapping;
    std::vector<unsigned int> bucket_vertex_sizes;

    // for dynamic updates
    std::vector<std::vector<unsigned int>> edge_contained_simple;
    std::vector<std::vector<std::pair<unsigned int, Time>>> vertex_contained_in;
    std::vector<std::vector<TE>> edges;

    // for edgelist version
    std::vector<std::vector<TE>> edgeSubstreams;

    std::vector<std::vector<unsigned int>> tailsSets;
    std::vector<std::vector<unsigned int>> headsSets;
    std::vector<std::vector<unsigned int>> timesSets;
    std::vector<std::vector<unsigned int>> transitiontimesSets;

    TimeSkipIndex timeSkipIndex;
    [[nodiscard]] unsigned long getStartPosition(NodeId nid) const {
        return timeSkipIndex[nid];
    }
};

void computeStartPositions(TemporalGraphStream const &tgs, Index &index, unsigned int &positions);

std::pair<std::vector<unsigned int>,std::vector<unsigned int>> getAllPossibleEdgesWithMinHash(TemporalGraphStream &tgs, NodeId u, unsigned int start_pos, unsigned int k);
std::pair<std::vector<unsigned int>,std::vector<unsigned int>> getAllPossibleEdgesWithMinHash(TemporalGraphStream &tgs, TemporalGraph &tg, NodeId u, unsigned int start_pos, unsigned int k, unsigned int med_deg);

void initPerm(unsigned long num_nodes);

std::pair<double, double>  getSize(Index const &index);


#endif //TPINDEX2_EAINDEX_H
