#include <cmath>
#include "temporalgraph/TemporalGraphStream.h"
#include "index/EAIndex.h"
#include "helper/Params.h"
#include "index/EAIndexEM.h"
#include "queries/FastestPathsQueries.h"
#include "queries/EarliestArrivalQueries.h"
#include "index/EdgeSet.h"
#include "helper/StatsPrinter.h"
#include "queries/InsertDelete.h"
#include "queries/EarliestArrivalQueriesIntervals.h"
#include "queries/EarliestArrivalQueriesOnlineLoad.h"
#include "index/EAIndexGreedy.h"

using namespace std;

int main(int argc, char *argv[]) {
    vector<string> args;
    for (size_t i = 1; i < argc; ++i) args.emplace_back(argv[i]);

    Params params;
    if (!params.parseArgs(args)) {
        show_help();
        return 1;
    }
    srand(params.rnd_seed);

    TemporalGraphStream tgs = loadTGS(params.dataset_path, params.directed);


    if (params.mode == Params::TG_STATS) {
        print_tg_size(tgs);
        return 0;
    }

    if (params.mode == Params::BASELINES_ONLY) {
        params.highest_degree_queries = false;
        SGLog::log() << "random queries experiment (baselines)\n" << endl;
        run_ea_experiment_baselines_only(tgs, params);
        run_fp_experiment_only_baselines(tgs, params);

        return 0;
    }


    Index index;
    if (!params.enable_dynamic) {
        if (!params.use_greedy)
            index = construct_index_EM2(tgs, params);
        else
            index = construct_index_greedy_batchwise(tgs, params);
    } else {
        index = construct_index_EM2(tgs, params);
        print_index_info(index, params, tgs);

        auto index2 = index;
        srand(params.rnd_seed);
        deleteEdgesExperiment2(tgs, index, params);

        srand(params.rnd_seed);
        insertEdgesExperiment2(tgs, index2, params);

        exit(0);
    }

    print_index_info(index, params, tgs);

    srand(params.rnd_seed);
    switch (params.index) {
        case Params::EA_AND_FP : {
            params.highest_degree_queries = false;
            SGLog::log() << "random queries experiment (index)\n" << endl;
            run_ea_experiment(tgs, params, index);
            run_fp_experiment(tgs, params, index);

            params.highest_degree_queries = true;
            SGLog::log() << "high degree queries experiment (index)\n" << endl;
            run_ea_experiment(tgs, params, index);
            run_fp_experiment(tgs, params, index);

            break;
        }
        case Params::EA : {
            run_ea_experiment(tgs, params, index);
            break;
        }
        case Params::EA_INTERVALS : {
            run_ea_experiment_with_intervals(tgs, params, index);
            break;
        }
        case Params::CLOSENESS : {
            run_closeness_experiment(tgs, index, params.closeness_mode);
            break;
        }
        case Params::FASTEST : {
            run_fp_experiment(tgs, params, index);
            break;
        }
        case Params::ONLINE_MODE : {
            run_ea_experiment_OnlineLoad(tgs, params, index);
            break;
        }
    }

    return 0;
}
