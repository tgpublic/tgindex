#ifndef TPINDEX2_EARLIESTARRIVALQUERIESAAAA_H
#define TPINDEX2_EARLIESTARRIVALQUERIESAAAA_H

#include "../temporalgraph/TemporalGraphStream.h"
#include "../index/EAIndex.h"
#include "../helper/Params.h"
#include <map>

void run_ea_experiment_with_intervals(TemporalGraphStream const &tgs, Params const &params, Index &index);

#endif //TPINDEX2_EARLIESTARRIVALQUERIESAAAA_H
