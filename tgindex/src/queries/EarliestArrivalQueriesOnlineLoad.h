#ifndef TPINDEX2_EARLIESTARRIVALQUERIESONLINELOAD_H
#define TPINDEX2_EARLIESTARRIVALQUERIESONLINELOAD_H

#include "../temporalgraph/TemporalGraphStream.h"
#include "../index/EAIndex.h"
#include "../helper/Params.h"
#include <map>

void run_ea_experiment_OnlineLoad(TemporalGraphStream const &tgs, Params const &params, Index &index);

#endif //TPINDEX2_EARLIESTARRIVALQUERIESONLINELOAD_H
