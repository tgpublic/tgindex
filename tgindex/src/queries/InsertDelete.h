#ifndef TPINDEX2_INSERTDELETE_H
#define TPINDEX2_INSERTDELETE_H

#include "../index/EAIndex.h"

void deleteEdgesExperiment2(TemporalGraphStream const &tgs, Index &index, Params const &paras);

void insertEdgesExperiment2(TemporalGraphStream const &tgs, Index &index, Params const &paras);

#endif //TPINDEX2_INSERTDELETE_H
