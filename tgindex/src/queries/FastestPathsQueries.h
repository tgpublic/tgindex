#ifndef TPINDEX2_FASTESTPATHSQUERIES_H
#define TPINDEX2_FASTESTPATHSQUERIES_H

#include "../temporalgraph/TemporalGraphStream.h"
#include "../index/EAIndex.h"
#include "../helper/Params.h"

void run_fp_experiment(TemporalGraphStream const &tgs, Params const &params, Index &index);
void run_fp_experiment_only_baselines(TemporalGraphStream const &tgs, Params const &params);

std::vector<NodeId> getRandomNodes(TemporalGraphStream const &tgs, unsigned int num_queries);
void run_closeness_experiment(TemporalGraphStream const &tgs, Index &index, int closeness_mode);

std::vector<std::pair<Time,Time>> getRandomIntervals(TemporalGraphStream const &tgs, unsigned int num_queries, unsigned int interval_div);

std::vector<Time> fastestPathNoIndex(TemporalGraphStream const &tgs, NodeId nid, Time interval_start, Time interval_end);
std::vector<Time> fastestPathBothIndex(TemporalGraphStream const &tgs, Index const &index, NodeId nid, Time interval_start, Time interval_end);
std::vector<Time> fastestPathTimeSkipIndexOnly(TemporalGraphStream const &tgs, TimeSkipIndex const & tsi, NodeId nid, Time interval_start, Time interval_end);
std::vector<Time> fastestPath(const TemporalGraph &tg, NodeId nid, Time interval_start, Time interval_end);


class LabelPQSP2 {

public:

    ~LabelPQSP2() {
        data.clear();
    }

    void push(const std::shared_ptr<label> &t) {
        data.push_back(t);
        t->pq_pos = data.size() - 1;
        heapifyUp(data.size() - 1);
    }

    std::shared_ptr<label>& top() {
        return data[0];
    }

    bool empty() {
        return data.empty();
    }

    size_t size() {
        return data.size();
    }

    void pop() {
        data[0] = data.back();
        data[0]->pq_pos = 0;
        data.pop_back();
        heapifyDown();
    }

private:

    std::vector<std::shared_ptr<label>> data;

    void heapifyUp(size_t p) {
        size_t child = p;
        size_t parent = getParent(child);

        while (child >= 0 && parent >= 0 && data[child]->d < data[parent]->d) {
            swap(child, parent);
            child = parent;
            parent = getParent(child);
        }
    }

    void heapifyDown(size_t p = 0) {
        int length = data.size();
        while (true) {
            int left = getLeftChild(p);
            int right = getRightChild(p);
            int smallest = p;

            if (left < length && data[left]->d < data[smallest]->d )
                smallest = left;

            if (right < length && data[right]->d < data[smallest]->d )
                smallest = right;

            if (smallest != p) {
                swap(smallest, p);
                p = smallest;
            } else break;
        }
    }

    static size_t getParent(size_t child) {
        if (child == 0) return 0;
        if (child % 2 == 0)
            return (child / 2) - 1;
        else
            return child / 2;
    }

    static size_t getLeftChild(size_t parent){
        return 2 * parent + 1;
    }

    static size_t getRightChild(size_t parent){
        return 2 * parent + 2;
    }

    void swap(size_t a, size_t b) {
        data[a]->pq_pos = b;
        data[b]->pq_pos = a;
        data[a].swap(data[b]);
    }

};


#endif //TPINDEX2_FASTESTPATHSQUERIES_H
