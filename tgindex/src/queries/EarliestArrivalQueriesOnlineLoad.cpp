#include "EarliestArrivalQueriesOnlineLoad.h"
#include "EarliestArrivalQueries.h"
#include "FastestPathsQueries.h"
#include "../helper/SGLog.h"

using namespace std;

std::vector<Time> earliestPathBothIndexOnlineLoad(TemporalGraphStream const &tgs, Index const &index, NodeId u, Time intervalStart, Time intervalEnd) {

    vector<Time> arr(tgs.num_nodes, inf);
    arr[u] = 0;
    auto c = index.nodeEdgeSetMapping[u];
    if (c == index.edgeSets.size()) return arr;
    auto start_pos = index.getStartPosition(u);


    for (unsigned int i = start_pos; i < index.edgeSets[c].size(); ++i) {
        if (arr[tgs.tails[index.edgeSets[c][i]]] < inf && arr[tgs.tails[index.edgeSets[c][i]]] <= tgs.times[index.edgeSets[c][i]]) {
            if (arr[tgs.heads[index.edgeSets[c][i]]] > tgs.times[index.edgeSets[c][i]] + tgs.transition_times[index.edgeSets[c][i]]) {
                arr[tgs.heads[index.edgeSets[c][i]]] = tgs.times[index.edgeSets[c][i]] + tgs.transition_times[index.edgeSets[c][i]];
            }
        }
    }
    return arr;
}

std::vector<Time> earliestPathSubstreamIndexOnlyOnlineLoad(TemporalGraphStream const &tgs, Index const &index, NodeId u, Time intervalStart, Time intervalEnd) {
    vector<Time> arr(tgs.num_nodes, inf);
    arr[u] = 0;
    auto c = index.nodeEdgeSetMapping[u];
    if (c == index.edgeSets.size()) return arr;

    for (unsigned int i : index.edgeSets[c]) {
        if (arr[tgs.tails[i]] < inf && arr[tgs.tails[i]] <= tgs.times[i]) {
            if (arr[tgs.heads[i]] > tgs.times[i] + tgs.transition_times[i]) {
                arr[tgs.heads[i]] = tgs.times[i] + tgs.transition_times[i];
            }
        }
    }
    return arr;
}


void run_ea_experiment_OnlineLoad(TemporalGraphStream const &tgs, Params const &params, Index &index) {
    SGLog::log() << "Runnig earliest path experiments" << endl;

    Timer timer;
    unsigned long reached1 = 0;
    unsigned long reached2 = 0;
    unsigned long reached3 = 0;
    unsigned long reached4 = 0;
    unsigned long reached5 = 0;
    unsigned long reached7 = 0;
    vector<double> times;

    auto randomNodes = getRandomNodes(tgs, 1000);
    double mean, stdev;

    Time intervalStart = 0;
    Time intervalEnd = inf;
    SGLog::log() << intervalStart << " " << intervalEnd << endl;

    SGLog::log() << "Run earliest path substream only" << endl;
    times.clear();
    for (auto &randomNode : randomNodes) {
        timer.start();
        auto result_index = earliestPathSubstreamIndexOnlyOnlineLoad(tgs, index, randomNode, intervalStart, intervalEnd);
        auto time = timer.stop();
        times.push_back(time);
        for (auto &r : result_index) {
            if (r != inf) reached1++;
        }
    }
    mean = 0; stdev = 0;
    get_mean_std(times, mean, stdev);
    auto total_time = std::accumulate(times.begin(), times.end(), 0.0);
    SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;

    SGLog::log() << "Run earliest path timeskip only" << endl;
    timer.start();
    TimeSkipIndex tsi(tgs.num_nodes, inf);
    for (unsigned int i = 0; i < tgs.tails.size(); ++i) {
        if (tsi[tgs.tails[i]] == inf) {
            tsi[tgs.tails[i]] = i;
        }
    }
    auto time = timer.stop();
    SGLog::log() << "Time for computing TSI: " << time << endl;
    times.clear();
    for (auto &randomNode : randomNodes) {
        timer.start();
        vector<Time> result_index = earliestPathTimeSkipIndexOnly(tgs, tsi, randomNode, intervalStart, intervalEnd);
        auto time = timer.stop();
        times.push_back(time);
        for (auto &r : result_index) {
            if (r != inf) reached2++;
        }
    }
    mean = 0; stdev = 0;
    get_mean_std(times, mean, stdev);
    total_time = std::accumulate(times.begin(), times.end(), 0.0);
    SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;

    SGLog::log() << "Run earliest path substream+timeskip" << endl;
    times.clear();
    for (auto &randomNode : randomNodes) {
        timer.start();
        vector<Time> result_index = earliestPathBothIndexOnlineLoad(tgs, index, randomNode, intervalStart, intervalEnd);
        auto time = timer.stop();
        times.push_back(time);
        for (auto &r : result_index) {
            if (r != inf) reached3++;
        }
    }
    mean = 0; stdev = 0;
    get_mean_std(times, mean, stdev);
    total_time = std::accumulate(times.begin(), times.end(), 0.0);
    SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;

    SGLog::log() << "Run earliest path no index" << endl;
    times.clear();
    for (auto &randomNode : randomNodes) {
        timer.start();
        auto result_index = earliestPathNoIndex(tgs, randomNode, intervalStart, intervalEnd);
        auto time = timer.stop();
        times.push_back(time);
        for (auto &r : result_index) {
            if (r != inf) reached4++;
        }
    }
    mean = 0; stdev = 0;
    get_mean_std(times, mean, stdev);
    total_time = std::accumulate(times.begin(), times.end(), 0.0);
    SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;

    SGLog::log() << "Run earliest path no index, Adjacency list version" << endl;
    TemporalGraph tg = toTemporalGraph(tgs);
    times.clear();
    for (auto &randomNode : randomNodes) {
        timer.start();
        auto result_index = earliestPathTG(tg, randomNode, intervalStart, intervalEnd);
        auto time = timer.stop();
        times.push_back(time);
        for (auto &r : result_index) {
            if (r != inf) reached5++;
        }
    }
    mean = 0; stdev = 0;
    get_mean_std(times, mean, stdev);
    total_time = std::accumulate(times.begin(), times.end(), 0.0);
    SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;

    SGLog::log() << "Run earliest path no index, Adjacency list version, Paper Xuan" << endl;
    TemporalGraph2 tg2(tgs);
    times.clear();
    for (auto &randomNode : randomNodes) {
        timer.start();
        auto result_index = earliestPathTG2(tg2, randomNode, intervalStart, intervalEnd);
        auto time = timer.stop();
        times.push_back(time);
        for (auto &r : result_index) {
            if (r != inf) reached7++;
        }
    }
    mean = 0; stdev = 0;
    get_mean_std(times, mean, stdev);
    total_time = std::accumulate(times.begin(), times.end(), 0.0);
    SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;

}

