#ifndef TPINDEX2_EARLIESTARRIVALQUERIES_H
#define TPINDEX2_EARLIESTARRIVALQUERIES_H

#include "../temporalgraph/TemporalGraphStream.h"
#include "../index/EAIndex.h"
#include "../helper/Params.h"
#include <map>
#include <list>

struct TemporalEdge2 {
    NodeId u_id, v_id;
    Time t, traversal_time;
};

std::pair<TemporalEdge2, bool> getEdge(std::vector<TemporalEdge2> edges_to_v, Time t);

struct TGNode2 {
    NodeId id = 0;
    std::list<std::vector<TemporalEdge2>> adjlist;

    std::unordered_map<NodeId, std::vector<TemporalEdge2>> _adjlist_map;
};

using TGNodes2 = std::vector<TGNode2>;

struct TemporalGraph2 {
    explicit TemporalGraph2(TemporalGraphStream const &tgs){
        num_nodes = tgs.num_nodes;
        for (NodeId nid = 0; nid < num_nodes; ++nid) {
            TGNode2 n;
            n.id = nid;
            nodes.push_back(n);
        }
        std::vector<TemporalEdge2> edges;
        std::vector<uint> num_neighbors(num_nodes, 0);
        for (uint i = 0; i < tgs.tails.size(); ++i) {
            TemporalEdge2 e{tgs.tails[i], tgs.heads[i], tgs.times[i], tgs.transition_times[i]};
            nodes.at(e.u_id)._adjlist_map[e.v_id].push_back(e);
        }
        std::vector<std::pair<uint,std::vector<TemporalEdge2>>> edges_at_node(num_nodes);
        std::vector<uint> cur(num_nodes, 0);
        for (auto &n : nodes) {
            for (auto &v : n._adjlist_map) {
                sort(v.second.begin(), v.second.end(), []
                        (TemporalEdge2 const &e, TemporalEdge2 const &f) { return e.t < f.t; });
                n.adjlist.emplace_back();
                for (auto &x : v.second){
                    n.adjlist.back().push_back(x);
                }
            }
            n._adjlist_map.clear();
        }
    }
    TGNodes2 nodes;
    unsigned int num_nodes;
};


void run_ea_experiment(TemporalGraphStream const &tgs, Params const &params, Index &index);

void run_ea_experiment_baselines_only(TemporalGraphStream const &tgs, Params const &params);

std::vector<Time> earliestPathTimeSkipIndexOnly(TemporalGraphStream const &tgs, TimeSkipIndex const & tsi, NodeId u, Time intervalStart, Time intervalEnd);

std::vector<Time> earliestPathTG(TemporalGraph const &tg, NodeId u, Time intervalStart, Time intervalEnd);

std::vector<Time> earliestPathTG2(TemporalGraph2 const &tg, NodeId u, Time intervalStart, Time intervalEnd);

std::vector<Time> earliestPathNoIndex(TemporalGraphStream const &tgs, NodeId u, Time intervalStart, Time intervalEnd);

std::vector<Time> earliestPathBothIndex(TemporalGraphStream const &tgs, Index const &index, NodeId u, Time intervalStart, Time intervalEnd);

#endif //TPINDEX2_EARLIESTARRIVALQUERIES_H
