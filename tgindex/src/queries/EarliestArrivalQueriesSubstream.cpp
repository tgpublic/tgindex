#include "EarliestArrivalQueries.h"
#include "FastestPathsQueries.h"
#include "../helper/SGLog.h"
#include "../index/EAIndexEMDynamic.h"

using namespace std;

std::vector<Time> earliestPathBothIndex1(TemporalGraphStream const &tgs, DynamicIndex const &index, NodeId u, Time intervalStart, Time intervalEnd) {

    vector<Time> arr(tgs.num_nodes, inf);
    arr[u] = 0;
    auto c = index.nodeEdgeSetMapping[u];
    if (c == index.edgeSets.size()) return arr;
    auto start_pos = index.getStartPosition(u);

    for (unsigned int i = start_pos; i < index.tailsSets[c].size(); ++i) {
        if (arr[index.tailsSets[c][i]] < inf && arr[index.tailsSets[c][i]] <= index.timesSets[c][i]) {
            if (arr[index.headsSets[c][i]] > index.timesSets[c][i] + index.transitiontimesSets[c][i]) {
                arr[index.headsSets[c][i]] = index.timesSets[c][i] + index.transitiontimesSets[c][i];
            }
        }
    }
    return arr;
}


void run_ea_experiment_dynamic(TemporalGraphStream const &tgs, Params const &params, DynamicIndex &dindex) {
    SGLog::log() << "Runnig earliest path experiments" << endl;

    Timer timer;
    unsigned long reached3 = 0;
    vector<double> times;

    auto randomNodes = getRandomNodes(tgs, 1000);
    double mean, stdev;

    Time intervalStart = 0;
    Time intervalEnd = inf;

    SGLog::log() << "Run earliest path substream+timeskip" << endl;
    times.clear();
    for (auto &randomNode : randomNodes) {
        loadQueryVertex(dindex, randomNode);

        timer.start();
        vector<Time> result_index = earliestPathBothIndex1(tgs, dindex, randomNode, intervalStart, intervalEnd);
        auto time = timer.stop();
        times.push_back(time);
        for (auto &r : result_index) {
            if (r != inf) reached3++;
        }
    }
    mean = 0; stdev = 0;
    get_mean_std(times, mean, stdev);
    auto total_time = std::accumulate(times.begin(), times.end(), 0.0);
    SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;

    cout << reached3 << endl << endl;
}
