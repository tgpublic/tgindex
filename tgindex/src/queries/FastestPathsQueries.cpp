#include "FastestPathsQueries.h"
#include <set>
#include <random>
#include "../helper/SGLog.h"
#include "../index/EAIndexEM.h"
#include <list>

using namespace std;

vector<Time> fastestPathNoIndex(TemporalGraphStream const &tgs, NodeId nid, Time interval_start, Time interval_end) {
    vector<Time> f_time(tgs.num_nodes, inf);
    vector<set<pair<Time, Time>>> ft_timepair(tgs.num_nodes, set<pair<Time, Time>>());
    f_time[nid]=0;
    set < pair< Time, Time > >::iterator it_tp, it_tp_low, it_tp_up, it_tp_1, it_tp_2;

    for (unsigned long epos = 0; epos < tgs.tails.size(); ++epos) {

        auto u_id = tgs.tails[epos];
        auto v_id = tgs.heads[epos];
        auto et = tgs.times[epos];
        auto etraversal_time = tgs.transition_times[epos];

        if(et<interval_end){
            if (et+etraversal_time<=interval_end && et >=interval_start){
                if (u_id == nid){
                    ft_timepair[u_id].insert(make_pair(et, et));
                }

                if(!ft_timepair[u_id].empty()){ // the path from x to u is not empty
                    it_tp=ft_timepair[u_id].upper_bound(make_pair(et, inf));

                    if(it_tp != ft_timepair[u_id].begin()){ //exists some pair arrived at or before t
                        it_tp --;

                        Time a_t=et+etraversal_time;
                        Time s_t=it_tp->second;

                        if(a_t-s_t < f_time[v_id])
                            f_time[v_id] = a_t-s_t;

                        if(!ft_timepair[v_id].empty()){ // the path from x to v is not empty
                            it_tp_1=ft_timepair[v_id].lower_bound(make_pair(a_t, s_t));

                            if(it_tp_1 == ft_timepair[v_id].begin() ){ // a_t is the smallest arrival time
                                if (it_tp_1->first>a_t){ // the arrival time of it_tp_1 is larger than a_t
                                    it_tp_low=it_tp_1;
                                    for(it_tp_up=it_tp_low; it_tp_up != ft_timepair[v_id].end(); it_tp_up++){
                                        if(it_tp_up->second > s_t){
                                            break;
                                        }
                                    }

                                    ft_timepair[v_id].erase(it_tp_low,it_tp_up); //remove useless pairs
                                    ft_timepair[v_id].insert(make_pair(a_t, s_t));
                                }
                            }
                            else if (it_tp_1 == ft_timepair[v_id].end()) { //a_t is the largest arrival time
                                it_tp_2 = it_tp_1;
                                it_tp_2 --;
                                if(it_tp_2->first == a_t) {
                                    ft_timepair[v_id].erase(it_tp_2);
                                    ft_timepair[v_id].insert(make_pair(a_t, s_t));
                                }
                                else if (it_tp_2->second < s_t){
                                    ft_timepair[v_id].insert(make_pair(a_t, s_t));
                                }
                            }
                            else {
                                if(it_tp_1->first > a_t) { // it_tp_1->first > a_t
                                    it_tp_2=it_tp_1;
                                    it_tp_2--;

                                    if(it_tp_2->first == a_t) {
                                        it_tp_low=it_tp_1;
                                        for(it_tp_up=it_tp_low; it_tp_up != ft_timepair[v_id].end(); it_tp_up++){
                                            if(it_tp_up->second > s_t){
                                                break;
                                            }
                                        }

                                        ft_timepair[v_id].erase(it_tp_2, it_tp_up);
                                        ft_timepair[v_id].insert(make_pair(a_t, s_t));
                                    }
                                    else if (it_tp_2->second < s_t){ // it_tp_2->first < a_t
                                        it_tp_low=it_tp_1;
                                        for(it_tp_up=it_tp_low; it_tp_up != ft_timepair[v_id].end(); it_tp_up++){
                                            if(it_tp_up->second > s_t){
                                                break;
                                            }
                                        }

                                        ft_timepair[v_id].erase(it_tp_low, it_tp_up);
                                        ft_timepair[v_id].insert(make_pair(a_t, s_t));
                                    }
                                }
                            }
                        }
                        else {
                            ft_timepair[v_id].insert(make_pair(a_t, s_t));
                        }

                    }
                }


            }
        }
        else {
            break;
        }
    }

    return f_time;
}

vector<Time> fastestPathBothIndex(TemporalGraphStream const &tgs, Index const &index, NodeId nid, Time interval_start, Time interval_end) {
    vector<Time> f_time(tgs.num_nodes, inf);
    vector<set<pair<Time, Time>>> ft_timepair(tgs.num_nodes, set<pair<Time, Time>>());
    f_time[nid]=0;

    auto c = index.nodeEdgeSetMapping[nid];
    if (c == index.edgeSets.size()) return f_time;

    set < pair< Time, Time > >::iterator it_tp, it_tp_low, it_tp_up, it_tp_1, it_tp_2;

    auto start_pos = index.getStartPosition(nid);

    for (unsigned int epos = start_pos; epos < index.tailsSets[c].size(); ++epos) {

        auto u_id = index.tailsSets[c][epos];
        auto v_id = index.headsSets[c][epos];
        auto et = index.timesSets[c][epos];
        auto etraversal_time = index.transitiontimesSets[c][epos];

        if(et<interval_end){
            if (et+etraversal_time<=interval_end && et >=interval_start){
                if (u_id == nid){
                    ft_timepair[u_id].insert(make_pair(et, et));
                }

                if(!ft_timepair[u_id].empty()){ // the path from x to u is not empty
                    it_tp=ft_timepair[u_id].upper_bound(make_pair(et, inf));

                    if(it_tp != ft_timepair[u_id].begin()){ //exists some pair arrived at or before t
                        it_tp --;

                        Time a_t=et+etraversal_time;
                        Time s_t=it_tp->second;

                        if(a_t-s_t < f_time[v_id])
                            f_time[v_id] = a_t-s_t;

                        if(!ft_timepair[v_id].empty()){ // the path from x to v is not empty
                            it_tp_1=ft_timepair[v_id].lower_bound(make_pair(a_t, s_t));

                            if(it_tp_1 == ft_timepair[v_id].begin() ){ // a_t is the smallest arrival time
                                if (it_tp_1->first>a_t){ // the arrival time of it_tp_1 is larger than a_t
                                    it_tp_low=it_tp_1;
                                    for(it_tp_up=it_tp_low; it_tp_up != ft_timepair[v_id].end(); it_tp_up++){
                                        if(it_tp_up->second > s_t){
                                            break;
                                        }
                                    }

                                    ft_timepair[v_id].erase(it_tp_low,it_tp_up); //remove useless pairs
                                    ft_timepair[v_id].insert(make_pair(a_t, s_t));
                                }
                            }
                            else if (it_tp_1 == ft_timepair[v_id].end()) { //a_t is the largest arrival time
                                it_tp_2 = it_tp_1;
                                it_tp_2 --;
                                if(it_tp_2->first == a_t) {
                                    ft_timepair[v_id].erase(it_tp_2);
                                    ft_timepair[v_id].insert(make_pair(a_t, s_t));
                                }
                                else if (it_tp_2->second < s_t){
                                    ft_timepair[v_id].insert(make_pair(a_t, s_t));
                                }
                            }
                            else {
                                if(it_tp_1->first > a_t) { // it_tp_1->first > a_t
                                    it_tp_2=it_tp_1;
                                    it_tp_2--;

                                    if(it_tp_2->first == a_t) {
                                        it_tp_low=it_tp_1;
                                        for(it_tp_up=it_tp_low; it_tp_up != ft_timepair[v_id].end(); it_tp_up++){
                                            if(it_tp_up->second > s_t){
                                                break;
                                            }
                                        }

                                        ft_timepair[v_id].erase(it_tp_2, it_tp_up);
                                        ft_timepair[v_id].insert(make_pair(a_t, s_t));
                                    }
                                    else if (it_tp_2->second < s_t){ // it_tp_2->first < a_t
                                        it_tp_low=it_tp_1;
                                        for(it_tp_up=it_tp_low; it_tp_up != ft_timepair[v_id].end(); it_tp_up++){
                                            if(it_tp_up->second > s_t){
                                                break;
                                            }
                                        }

                                        ft_timepair[v_id].erase(it_tp_low, it_tp_up);
                                        ft_timepair[v_id].insert(make_pair(a_t, s_t));
                                    }
                                }
                            }
                        }
                        else {
                            ft_timepair[v_id].insert(make_pair(a_t, s_t));
                        }

                    }
                }


            }
        }
        else {
            break;
        }
    }

    return f_time;
}

vector<Time> fastestPathTimeSkipIndexOnly(TemporalGraphStream const &tgs, TimeSkipIndex const & tsi, NodeId nid, Time interval_start, Time interval_end) {
    vector<Time> f_time(tgs.num_nodes, inf);
    vector<set<pair<Time, Time>>> ft_timepair(tgs.num_nodes, set<pair<Time, Time>>());
    f_time[nid]=0;
    set < pair< Time, Time > >::iterator it_tp, it_tp_low, it_tp_up, it_tp_1, it_tp_2;

    for (unsigned long epos = tsi[nid]; epos < tgs.tails.size(); ++epos) {

        auto u_id = tgs.tails[epos];
        auto v_id = tgs.heads[epos];
        auto et = tgs.times[epos];
        auto etraversal_time = tgs.transition_times[epos];

        if(et<interval_end){
            if (et+etraversal_time<=interval_end && et >=interval_start){
                if (u_id == nid){
                    ft_timepair[u_id].insert(make_pair(et, et));
                }

                if(!ft_timepair[u_id].empty()){ // the path from x to u is not empty
                    it_tp=ft_timepair[u_id].upper_bound(make_pair(et, inf));

                    if(it_tp != ft_timepair[u_id].begin()){ //exists some pair arrived at or before t
                        it_tp --;

                        Time a_t=et+etraversal_time;
                        Time s_t=it_tp->second;

                        if(a_t-s_t < f_time[v_id])
                            f_time[v_id] = a_t-s_t;

                        if(!ft_timepair[v_id].empty()){ // the path from x to v is not empty
                            it_tp_1=ft_timepair[v_id].lower_bound(make_pair(a_t, s_t));

                            if(it_tp_1 == ft_timepair[v_id].begin() ){ // a_t is the smallest arrival time
                                if (it_tp_1->first>a_t){ // the arrival time of it_tp_1 is larger than a_t
                                    it_tp_low=it_tp_1;
                                    for(it_tp_up=it_tp_low; it_tp_up != ft_timepair[v_id].end(); it_tp_up++){
                                        if(it_tp_up->second > s_t){
                                            break;
                                        }
                                    }

                                    ft_timepair[v_id].erase(it_tp_low,it_tp_up); //remove useless pairs
                                    ft_timepair[v_id].insert(make_pair(a_t, s_t));
                                }
                            }
                            else if (it_tp_1 == ft_timepair[v_id].end()) { //a_t is the largest arrival time
                                it_tp_2 = it_tp_1;
                                it_tp_2 --;
                                if(it_tp_2->first == a_t) {
                                    ft_timepair[v_id].erase(it_tp_2);
                                    ft_timepair[v_id].insert(make_pair(a_t, s_t));
                                }
                                else if (it_tp_2->second < s_t){
                                    ft_timepair[v_id].insert(make_pair(a_t, s_t));
                                }
                            }
                            else {
                                if(it_tp_1->first > a_t) { // it_tp_1->first > a_t
                                    it_tp_2=it_tp_1;
                                    it_tp_2--;

                                    if(it_tp_2->first == a_t) {
                                        it_tp_low=it_tp_1;
                                        for(it_tp_up=it_tp_low; it_tp_up != ft_timepair[v_id].end(); it_tp_up++){
                                            if(it_tp_up->second > s_t){
                                                break;
                                            }
                                        }

                                        ft_timepair[v_id].erase(it_tp_2, it_tp_up);
                                        ft_timepair[v_id].insert(make_pair(a_t, s_t));
                                    }
                                    else if (it_tp_2->second < s_t){ // it_tp_2->first < a_t
                                        it_tp_low=it_tp_1;
                                        for(it_tp_up=it_tp_low; it_tp_up != ft_timepair[v_id].end(); it_tp_up++){
                                            if(it_tp_up->second > s_t){
                                                break;
                                            }
                                        }

                                        ft_timepair[v_id].erase(it_tp_low, it_tp_up);
                                        ft_timepair[v_id].insert(make_pair(a_t, s_t));
                                    }
                                }
                            }
                        }
                        else {
                            ft_timepair[v_id].insert(make_pair(a_t, s_t));
                        }

                    }
                }


            }
        }
        else {
            break;
        }
    }

    return f_time;
}

vector<Time> fastestPath(const TemporalGraph &tg, NodeId nid, Time interval_start, Time interval_end) {

    vector<Time> minduration(tg.nodes.size(), inf);
    minduration[nid] = 0;

    vector<bool> visited(tg.nodes.size(), false);

    vector<list<shared_ptr<label>>> nodelabels(tg.nodes.size());
    auto l = std::make_shared<label>();
    l->a = l->s = l->d = 0;
    l->nid = nid;
    nodelabels[nid].push_back(l);

    LabelPQSP2 q;
    q.push(l);

    unsigned int num_finished = 1;

    while (!q.empty()) {

        auto cur = q.top();
        q.pop();
        if (cur->deleted) {
            continue;
        }

        if (!visited[cur->nid]) {
            visited[cur->nid] = true;
            minduration[cur->nid] = minduration[cur->nid] < cur->d ? minduration[cur->nid] : cur->d;
            num_finished++;
            if (num_finished == tg.num_nodes) break;
        }

        for (const TemporalEdge &e : tg.nodes[cur->nid].adjlist) {

            if (e.t < interval_start || e.t + e.traversal_time > interval_end) continue;

            if (e.t >= cur->a) {

                auto lnew = std::make_shared<label>();
                lnew->nid = e.v_id;
                if (cur->s == 0) lnew->s = e.t;
                else lnew->s = cur->s;
                lnew->a = e.t + e.traversal_time;
                lnew->d = lnew->a - lnew->s;

                bool dom = false;
                auto i = nodelabels[e.v_id].begin();
                while (i != nodelabels[e.v_id].end()) {
                    if (((*i)->s < lnew->s && (*i)->a >= lnew->a) || ((*i)->s == lnew->s && (*i)->a > lnew->a)) {
                        (*i)->deleted = true;
                        i = nodelabels[e.v_id].erase(i);
                        continue;
                    }
                    if (((*i)->s >= lnew->s && (*i)->a <= lnew->a) || ((*i)->s == lnew->s && (*i)->a <= lnew->a)) {
                        dom = true;
                        break;
                    }
                    ++i;
                }

                if (!dom) {
                    nodelabels[e.v_id].push_back(lnew);
                    minduration[e.v_id] = minduration[e.v_id] < lnew->d ? minduration[e.v_id] : lnew->d;
//                    if (tg.nodes[lnew->nid].maxTime >= e.t + e.traversal_time)
                        q.push(lnew);
                }
            }
        }
    }
    return minduration;
}


vector<NodeId> getRandomNodes(TemporalGraphStream const &tgs, unsigned int num_queries) {
    num_queries = min((unsigned int)num_queries, (unsigned int)tgs.num_nodes);
    vector<NodeId> randomNodes;
    randomNodes.reserve(num_queries);
    vector<bool> used(tgs.num_nodes, false);
    for (int i = 0; i < num_queries; ++i) {
        auto u = rand() % tgs.num_nodes;
        while (used[u]) u = rand() % tgs.num_nodes;
        used[u] = true;
        randomNodes.push_back(u);
    }
    SGLog::log() << endl;
    return randomNodes;
}


vector<pair<Time,Time>> getRandomIntervals(TemporalGraphStream const &tgs, unsigned int num_queries, unsigned int interval_div) {
    vector<pair<Time,Time>> randomIntervals;
    Time interval_min = tgs.times.front();
    Time interval_max = tgs.times.back();
    Time interval_length = (interval_max - interval_min) / interval_div;
    for (int i = 0; randomIntervals.size() < num_queries; ++i) {
        auto a = rand() % (interval_max - interval_length - interval_min) + interval_min;
        auto b = a + interval_length;
        randomIntervals.emplace_back(a, b);
    }
    return randomIntervals;
}


void run_fp_experiment(TemporalGraphStream const &tgs, Params const &params, Index &index) {
    SGLog::log() << "Runnig fastest path experiments" << endl;

    Timer timer;
    unsigned long reached1 = 0;
    unsigned long reached2 = 0;
    unsigned long reached3 = 0;
    unsigned long reached4 = 0;
    unsigned long reached5 = 0;
    vector<double> times;

    auto randomNodes = getRandomNodes(tgs, 1000);
    double mean, stdev;

    SGLog::log() << "Run fastest path substream+timeskip" << endl;
    times.clear();
    for (auto &randomNode : randomNodes) {
        loadQueryVertex(tgs, index, randomNode);

        timer.start();
        vector<Time> result_index = fastestPathBothIndex(tgs, index, randomNode, 0, inf);
        auto time = timer.stop();
        times.push_back(time);
        for (auto &r : result_index) {
            if (r != inf) reached3 += r;
        }
    }
    mean = 0; stdev = 0;
    get_mean_std(times, mean, stdev);
    auto total_time = std::accumulate(times.begin(), times.end(), 0.0);
    SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;

}

void run_fp_experiment_only_baselines(TemporalGraphStream const &tgs, Params const &params) {
    SGLog::log() << "Runnig fastest path experiments" << endl;

    Timer timer;
    unsigned long reached2 = 0;
    unsigned long reached4 = 0;
    unsigned long reached5 = 0;
    vector<double> times;

    auto randomNodes = getRandomNodes(tgs, 1000);
    double mean, stdev;

    SGLog::log() << "Run fastest path timeskip only" << endl;
    timer.start();
    TimeSkipIndex tsi(tgs.num_nodes, inf);
    for (unsigned int i = 0; i < tgs.tails.size(); ++i) {
        if (tsi[tgs.tails[i]] == inf) {
            tsi[tgs.tails[i]] = i;
        }
    }
    auto time = timer.stop();
    SGLog::log() << "Time for computing TSI: " << time << endl;
    times.clear();
    for (auto &randomNode : randomNodes) {
        timer.start();
        vector<Time> result_index = fastestPathTimeSkipIndexOnly(tgs, tsi, randomNode, 0, inf);
        auto time = timer.stop();
        times.push_back(time);
        for (auto &r : result_index) {
            if (r != inf) reached2 += r;
        }
    }
    mean = 0; stdev = 0;
    get_mean_std(times, mean, stdev);
    auto total_time = std::accumulate(times.begin(), times.end(), 0.0);
    SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;

    SGLog::log() << "Run fastest path no index" << endl;
    times.clear();
    for (auto &randomNode : randomNodes) {
        timer.start();
        auto result_index = fastestPathNoIndex(tgs, randomNode, 0, inf);
        auto time = timer.stop();
        times.push_back(time);
        for (auto &r : result_index) {
            if (r != inf) reached4 += r;
        }
    }
    mean = 0; stdev = 0;
    get_mean_std(times, mean, stdev);
    total_time = std::accumulate(times.begin(), times.end(), 0.0);
    SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;

    SGLog::log() << "Run fastest path adj list version" << endl;
    times.clear();
    auto tg = toTemporalGraph(tgs);
    for (auto &randomNode : randomNodes) {
        timer.start();
        auto result_index = fastestPath(tg, randomNode, 0, inf);
        auto time = timer.stop();
        times.push_back(time);
        for (auto &r : result_index) {
            if (r != inf) reached5 += r;
        }
    }
    mean = 0; stdev = 0;
    get_mean_std(times, mean, stdev);
    total_time = std::accumulate(times.begin(), times.end(), 0.0);
    SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;

}

void run_closeness_experiment(TemporalGraphStream const &tgs, Index &index, int closeness_mode) {
    SGLog::log() << "running top-k temporal closeness experiment" << endl;
    Timer timer;
    double closeness1 = 0;
    double closeness2 = 0;
    double closeness3 = 0;
    double closeness4 = 0;
    vector<double> times;

    double mean, stdev;

    if (closeness_mode == 1) {
        SGLog::log() << "Run closeness timeskip only" << endl;
        timer.start();
        TimeSkipIndex tsi(tgs.num_nodes, inf);
        for (unsigned int i = 0; i < tgs.tails.size(); ++i) {
            if (tsi[tgs.tails[i]] == inf) {
                tsi[tgs.tails[i]] = i;
            }
        }
        auto time1 = timer.stop();
        SGLog::log() << "Time for computing TSI: " << time1 << endl;
        times.clear();
        for (NodeId nid = 0; nid < tgs.num_nodes; nid++) {
            timer.start();
            vector<Time> result_index = fastestPathTimeSkipIndexOnly(tgs, tsi, nid, 0, inf);
            auto time = timer.stop();
            times.push_back(time);
            double closeness = 0;
            for (auto &r: result_index) {
                if (r != inf && r > 0) {
                    closeness += 1.0 / (double) r;
                }
            }
            closeness2 += closeness;
        }
        mean = 0;
        stdev = 0;
        get_mean_std(times, mean, stdev);
        auto total_time = std::accumulate(times.begin(), times.end(), 0.0);
        SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;
    }

    if (closeness_mode == 0) {
        SGLog::log() << "Run closeness substream+timeskip" << endl;
        times.clear();
        for (NodeId nid = 0; nid < tgs.num_nodes; nid++) {
            loadQueryVertex(tgs, index, nid);

            timer.start();
            vector<Time> result_index = fastestPathBothIndex(tgs, index, nid, 0, inf);
            auto time = timer.stop();
            times.push_back(time);
            double closeness = 0;
            for (auto &r: result_index) {
                if (r != inf && r > 0) {
                    closeness += 1.0 / (double) r;
                }
            }
            closeness3 += closeness;
        }
        mean = 0;
        stdev = 0;
        get_mean_std(times, mean, stdev);
        auto total_time = std::accumulate(times.begin(), times.end(), 0.0);
        SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;
    }

    if (closeness_mode == 2) {
        SGLog::log() << "Run closeness no index" << endl;
        times.clear();
        for (NodeId nid = 0; nid < tgs.num_nodes; nid++) {
            timer.start();
            auto result_index = fastestPathNoIndex(tgs, nid, 0, inf);
            auto time = timer.stop();
            times.push_back(time);
            double closeness = 0;
            for (auto &r: result_index) {
                if (r != inf && r > 0) {
                    closeness += 1.0 / (double) r;
                }
            }
            closeness4 += closeness;
        }
        mean = 0;
        stdev = 0;
        get_mean_std(times, mean, stdev);
        auto total_time = std::accumulate(times.begin(), times.end(), 0.0);
        SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;
    }
    cout << closeness1 << " " << closeness2 << " " << closeness3 << " " << closeness4 << endl << endl;
}


