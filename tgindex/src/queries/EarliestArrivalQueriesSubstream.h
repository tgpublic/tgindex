#ifndef TPINDEX2_EARLIESTARRIVALQUERIESSUBSTREAM_H
#define TPINDEX2_EARLIESTARRIVALQUERIESSUBSTREAM_H

#include "../temporalgraph/TemporalGraphStream.h"
#include "../index/EAIndex.h"
#include "../helper/Params.h"
#include "../index/EAIndexEMDynamic.h"

void run_ea_experiment_dynamic(TemporalGraphStream const &tgs, Params const &params, DynamicIndex &index);

#endif //TPINDEX2_EARLIESTARRIVALQUERIESSUBSTREAM_H
