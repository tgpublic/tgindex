
#include "EarliestArrivalQueries.h"
#include "FastestPathsQueries.h"
#include <cassert>
#include "../helper/SGLog.h"
#include "../index/EAIndexEM.h"
#include <memory>
#include <list>

using namespace std;


std::vector<Time> earliestPathBothIndex(TemporalGraphStream const &tgs, Index const &index, NodeId u, Time intervalStart, Time intervalEnd) {

    vector<Time> arr(tgs.num_nodes, inf);
    arr[u] = 0;
    auto c = index.nodeEdgeSetMapping[u];
    if (c == index.edgeSets.size()) return arr;
    auto start_pos = index.getStartPosition(u);

    for (unsigned int i = start_pos; i < index.tailsSets[c].size(); ++i) {
    if (arr[index.tailsSets[c][i]] < inf && arr[index.tailsSets[c][i]] <= index.timesSets[c][i]) {
            if (arr[index.headsSets[c][i]] > index.timesSets[c][i] + index.transitiontimesSets[c][i]) {
                arr[index.headsSets[c][i]] = index.timesSets[c][i] + index.transitiontimesSets[c][i];
            }
        }
    }
    return arr;
}


std::vector<Time> earliestPathNoIndex(TemporalGraphStream const &tgs, NodeId u, Time intervalStart, Time intervalEnd) {
    vector<Time> arr(tgs.num_nodes, inf);
    arr[u] = 0;
    for (unsigned long i = 0; i < tgs.tails.size(); ++i) {
        if (arr[tgs.tails[i]] <= tgs.times[i]) {
            if (arr[tgs.heads[i]] > tgs.times[i] + tgs.transition_times[i]) {
                arr[tgs.heads[i]] = tgs.times[i] + tgs.transition_times[i];
            }
        }
    }
    return arr;
}

std::vector<Time> earliestPathTimeSkipIndexOnly(TemporalGraphStream const &tgs, TimeSkipIndex const & tsi, NodeId u, Time intervalStart, Time intervalEnd) {
    vector<Time> arr(tgs.num_nodes, inf);
    arr[u] = 0;
    for (unsigned long i = tsi[u]; i < tgs.tails.size(); ++i) {
        if (arr[tgs.tails[i]] <= tgs.times[i]) {
            if (arr[tgs.heads[i]] > tgs.times[i] + tgs.transition_times[i]) {
                arr[tgs.heads[i]] = tgs.times[i] + tgs.transition_times[i];
            }
        }
    }
    return arr;
}


std::vector<Time> earliestPathTG(TemporalGraph const &tg, NodeId u, Time intervalStart, Time intervalEnd) {

    vector<shared_ptr<label>> nodelabels(tg.nodes.size(), nullptr);
    auto l = std::make_shared<label>();
    l->a = 0;
    l->nid = u;

    nodelabels[u] = l;
    LabelPQASP q;
    q.push(l);
    vector<Time> earliestArrival(tg.num_nodes, inf);
    earliestArrival[u] = 0;
    unsigned int result = 0;

    vector<bool> inQ(tg.nodes.size(), false);
    vector<bool> done(tg.nodes.size(), false);
    inQ[u] = true;

    while (!q.empty()) {
        auto cur = q.top();
        q.pop();

        inQ[cur->nid] = false;

        if (done[cur->nid]) continue;
        done[cur->nid] = true;

        for (const TemporalEdge &e : tg.nodes[cur->nid].adjlist) {
            if (e.t >= cur->a && !done[e.v_id]) {
                if (nodelabels[e.v_id] == nullptr) {
                    auto lnew = std::make_shared<label>();
                    lnew->a = e.t + e.traversal_time;
                    lnew->nid = e.v_id;
                    nodelabels[e.v_id] = lnew;
                    if (tg.nodes[e.v_id].maxTime >= e.t + e.traversal_time) {
                        q.push(lnew);
                        inQ[e.v_id] = true;
                    }
                    earliestArrival[e.v_id] = e.t + e.traversal_time;
                    ++result;
                } else {
                    if (earliestArrival[e.v_id] > e.t + e.traversal_time) {
                        earliestArrival[e.v_id] = e.t + e.traversal_time;
                        nodelabels[e.v_id]->a = e.t + e.traversal_time;

                        if (tg.nodes[nodelabels[e.v_id]->nid].maxTime >= e.t + e.traversal_time) {
                            if (!inQ[e.v_id]) {
                                q.push(nodelabels[e.v_id]);
                                inQ[e.v_id] = true;
                            } else
                                q.decreasedKey(nodelabels[e.v_id]->pq_pos);
                        }
                    }
                }
            }
        }
    }
    return earliestArrival;
}


std::pair<TemporalEdge2, bool> getEdge(std::vector<TemporalEdge2> edges_to_v, Time t){
    auto p = std::lower_bound(edges_to_v.begin(), edges_to_v.end(), t,
                              [](const TemporalEdge2& e1, Time value){
                                  return e1.t < value;
                              });
    if (p != edges_to_v.end()) {
        return {*p, true};
    }
    return {TemporalEdge2{}, false};
}

std::vector<Time> earliestPathTG2(TemporalGraph2 const &tg, NodeId u, Time intervalStart, Time intervalEnd) {

    vector<shared_ptr<label>> nodelabels(tg.nodes.size(), nullptr);
    auto l = std::make_shared<label>();
    l->a = 0;
    l->nid = u;

    nodelabels[u] = l;
    LabelPQASP q;
    q.push(l);
    vector<Time> earliestArrival(tg.num_nodes, inf);
    earliestArrival[u] = 0;
    unsigned int result = 0;

    vector<bool> inQ(tg.nodes.size(), false);
    inQ[u] = true;

    vector<bool> done(tg.nodes.size(), false);

    while (!q.empty()) {
        auto cur = q.top();
        q.pop();

        if (done[cur->nid]) continue;

        inQ[cur->nid] = false;
        done[cur->nid] = true;

        for (auto &edge_vec : tg.nodes[cur->nid].adjlist) {
            if (edge_vec.empty()) continue;
            if (done[edge_vec[0].v_id]) continue;
            auto [e,b] = getEdge(edge_vec, cur->a);
            if (!b) continue;

            if (e.t >= cur->a) {
                if (nodelabels[e.v_id] == nullptr) {
                    auto lnew = std::make_shared<label>();
                    lnew->a = e.t + e.traversal_time;
                    lnew->nid = e.v_id;
                    nodelabels[e.v_id] = lnew;
                    q.push(lnew);
                    inQ[e.v_id] = true;
                    earliestArrival[e.v_id] = e.t + e.traversal_time;
                    ++result;
                } else {
                    if (earliestArrival[e.v_id] > e.t + e.traversal_time) {
                        earliestArrival[e.v_id] = e.t + e.traversal_time;
                        nodelabels[e.v_id]->a = e.t + e.traversal_time;

                        if (!inQ[e.v_id]) {
                            q.push(nodelabels[e.v_id]);
                            inQ[e.v_id] = true;
                        } else
                            q.decreasedKey(nodelabels[e.v_id]->pq_pos);
                    }
                }
            }
        }
    }
    return earliestArrival;
}


void run_ea_experiment(TemporalGraphStream const &tgs, Params const &params, Index &index) {
    SGLog::log() << "Runnig earliest path experiments" << endl;

    Timer timer;
    unsigned long reached1 = 0;
    unsigned long reached2 = 0;
    unsigned long reached3 = 0;
    unsigned long reached4 = 0;
    unsigned long reached5 = 0;
    unsigned long reached6 = 0;
    unsigned long reached7 = 0;
    vector<double> times;

    auto randomNodes = getRandomNodes(tgs, 1000);
    double mean, stdev;

    Time intervalStart = 0;
    Time intervalEnd = inf;


    SGLog::log() << "Run earliest path substream+timeskip" << endl;
    times.clear();
    for (auto &randomNode : randomNodes) {
        loadQueryVertex(tgs, index, randomNode);

        timer.start();
        vector<Time> result_index = earliestPathBothIndex(tgs, index, randomNode, intervalStart, intervalEnd);
        auto time = timer.stop();
        times.push_back(time);
        for (auto &r : result_index) {
            if (r != inf) reached3++;
        }
    }
    mean = 0; stdev = 0;
    get_mean_std(times, mean, stdev);
    auto total_time = std::accumulate(times.begin(), times.end(), 0.0);
    SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;

}


void run_ea_experiment_baselines_only(TemporalGraphStream const &tgs, Params const &params) {
    SGLog::log() << "Runnig earliest path experiments" << endl;

    Timer timer;
    unsigned long reached2 = 0;
    unsigned long reached4 = 0;
    unsigned long reached5 = 0;
    unsigned long reached7 = 0;
    vector<double> times;

    auto randomNodes = getRandomNodes(tgs, 1000);
    double mean, stdev;

    Time intervalStart = 0;
    Time intervalEnd = inf;
    SGLog::log() << intervalStart << " " << intervalEnd << endl;

    SGLog::log() << "Run earliest path timeskip only" << endl;
    timer.start();
    TimeSkipIndex tsi(tgs.num_nodes, inf);
    for (unsigned int i = 0; i < tgs.tails.size(); ++i) {
        if (tsi[tgs.tails[i]] == inf) {
            tsi[tgs.tails[i]] = i;
        }
    }
    auto time = timer.stop();
    SGLog::log() << "Time for computing TSI: " << time << endl;
    times.clear();
    for (auto &randomNode : randomNodes) {
        timer.start();
        vector<Time> result_index = earliestPathTimeSkipIndexOnly(tgs, tsi, randomNode, intervalStart, intervalEnd);
        auto time = timer.stop();
        times.push_back(time);
        for (auto &r : result_index) {
            if (r != inf) reached2++;
        }
    }
    mean = 0;
    stdev = 0;
    get_mean_std(times, mean, stdev);
    auto total_time = std::accumulate(times.begin(), times.end(), 0.0);
    SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;

    SGLog::log() << "Run earliest path no index" << endl;
    times.clear();
    for (auto &randomNode : randomNodes) {
        timer.start();
        auto result_index = earliestPathNoIndex(tgs, randomNode, intervalStart, intervalEnd);
        auto time = timer.stop();
        times.push_back(time);
        for (auto &r : result_index) {
            if (r != inf) reached4++;
        }
    }
    mean = 0;
    stdev = 0;
    get_mean_std(times, mean, stdev);
    total_time = std::accumulate(times.begin(), times.end(), 0.0);
    SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;

    SGLog::log() << "Run earliest path no index, Adjacency list version" << endl;
    TemporalGraph tg = toTemporalGraph(tgs);
    times.clear();
    for (auto &randomNode : randomNodes) {
        timer.start();
        auto result_index = earliestPathTG(tg, randomNode, intervalStart, intervalEnd);
        auto time = timer.stop();
        times.push_back(time);
        for (auto &r : result_index) {
            if (r != inf) reached5++;
        }
    }
    mean = 0;
    stdev = 0;
    get_mean_std(times, mean, stdev);
    total_time = std::accumulate(times.begin(), times.end(), 0.0);
    SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;


    SGLog::log() << "Run earliest path no index, Adjacency list version, Paper Xuan" << endl;
    TemporalGraph2 tg2(tgs);
    times.clear();
    for (auto &randomNode : randomNodes) {
        timer.start();
        auto result_index = earliestPathTG2(tg2, randomNode, intervalStart, intervalEnd);
        auto time = timer.stop();
        times.push_back(time);
        for (auto &r : result_index) {
            if (r != inf) reached7++;
        }
    }
    mean = 0; stdev = 0;
    get_mean_std(times, mean, stdev);
    total_time = std::accumulate(times.begin(), times.end(), 0.0);
    SGLog::log() << "Total: " << total_time << "\t Average:" << mean << " +- " << stdev << endl;

    assert(reached2 == reached4);
    assert(reached2 == reached5);
    assert(reached2 == reached7);

}