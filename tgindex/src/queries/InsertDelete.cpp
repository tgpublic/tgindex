#include "InsertDelete.h"
#include "../helper/SGLog.h"
#include "EarliestArrivalQueriesSubstream.h"
#include "EarliestArrivalQueries.h"

using namespace std;

void deleteEdgesExperiment2(TemporalGraphStream const &tgs, Index &index, Params const &params) {
    Timer timer;
    double del_time = 0;
    SGLog::log() << "deleting edges" << endl;
    unordered_set<EdgeId> deleted;
    vector<EdgeId> deletedvec;


    auto dindex = toDynamicIndex(tgs, index);

    auto num_to_delete = 1000;
    for (int i = 0; i < num_to_delete; ++i) {
        EdgeId eid = rand() % tgs.tails.size();
        deleted.insert(eid);
        deletedvec.push_back(eid);
    }
    timer.start();
    delete_edges(dindex, deletedvec);
    del_time += timer.stop();
    SGLog::log() << "deleted edges in total time: " << del_time << endl;
    SGLog::log() << "deleted edges on average: " << del_time/double(num_to_delete) << endl;

    TemporalGraphStream tgsnew;
    for (unsigned int i = 0; i < tgs.tails.size(); ++i) {
        if (deleted.find(i) == deleted.end()) {
            tgsnew.tails.push_back(tgs.tails[i]);
            tgsnew.heads.push_back(tgs.heads[i]);
            tgsnew.times.push_back(tgs.times[i]);
            tgsnew.transition_times.push_back(tgs.transition_times[i]);
        }
    }
    tgsnew.num_nodes = tgs.num_nodes;

    srand(params.rnd_seed);
    run_ea_experiment_dynamic(tgs, params, dindex);
    dindex.clear();

    srand(params.rnd_seed);
    run_ea_experiment_baselines_only(tgsnew, params);
}

void insertEdgesExperiment2(TemporalGraphStream const &tgs, Index &index, Params const &params) {

    auto dindex = toDynamicIndex(tgs, index);
    index.edgeSets.clear();
    index.headsSets.clear();
    index.edges.clear();
    index.transitiontimesSets.clear();
    index.nodeEdgeSetMapping.clear();
    index.bucket_vertex_sizes.clear();
    index.edge_contained_simple.clear();
    index.timesSets.clear();
    index.tailsSets.clear();
    index.edgeSubstreams.clear();
    index.timeSkipIndex.clear();
    index.vertex_contained_in.clear();

    Timer timer;
    auto tgsnew = tgs;
    SGLog::log() << "inserting edges" << endl;
    struct TEdge {
        NodeId u,v;
        unsigned long t,tt;
    };
    vector<TEdge> inserted;
    for (unsigned int i = 0; i < tgsnew.tails.size(); ++i) {
        inserted.push_back({tgsnew.tails[i], tgsnew.heads[i], tgsnew.times[i], tgsnew.transition_times[i]});
    }
    double time = 0;
    vector<TE> to_insert;
    srand(params.rnd_seed);

    vector<bool> used(tgs.num_nodes, false);

    unsigned int num_to_insert = 1000;
    EdgeId eid = tgs.tails.size();
    for (int i = 0; i < num_to_insert; ++i) {
        NodeId u = rand() % tgs.num_nodes;
        NodeId v = rand() % tgs.num_nodes;
        while (used[u]) u = rand()%tgs.num_nodes;
        while (used[v]) v = rand()%tgs.num_nodes;
        used[u] = used[v] = true;
        if (u == v) continue;
        Time t = tgs.times[0] + (rand() % 10000);
        Time tt = 1;
        to_insert.push_back({u,v,t,tt});
        timer.start();
        TE te {u,v,t,tt,eid++};
        insert_edge(tgsnew, dindex, te);
        time += timer.stop();
        inserted.push_back({u,v,t,tt});
    }

    SGLog::log() << "added edges in total time: " << time << endl;
    SGLog::log() << "added edges on average: " << time/double(num_to_insert) << endl;
    TemporalGraphStream tgsnew2;
    sort(inserted.begin(), inserted.end(), [](auto const &e1, auto const &e2){return e1.t < e2.t;});
    for (auto &e : inserted) {
        tgsnew2.tails.push_back(e.u);
        tgsnew2.heads.push_back(e.v);
        tgsnew2.times.push_back(e.t);
        tgsnew2.transition_times.push_back(e.tt);
    }
    tgsnew2.num_nodes = tgs.num_nodes;

    srand(params.rnd_seed);
    run_ea_experiment_dynamic(tgs, params, dindex);

    srand(params.rnd_seed);
    run_ea_experiment_baselines_only(tgsnew2, params);
}