# TG Index

Source code for the paper: *"An Index for Single Source All Destinations Distance Queries in Temporal Graphs"*, 
Lutz Oettershagen and Petra Mutzel, 2022.

## Data sets

The data sets are in the zipped file `datasets.zip`.
In the folder `datasets` unzip the data sets with `unzip -j datasets.zip`.

## Folder structure

- `datasets/` contains the data sets (after unzipping)
- `scripts/` contains scripts for the experiments and plots
- `tgindex/` contains the C++ source code of our algorithms

## Build

Next, in the folder `tgindex` run:
```
mkdir cmake-build-release
cd cmake-build-release
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

After running the commands the program `tgindex` can be used. Run it without parameters to get help.

## Running the experiments

In the folder `scripts/slurm` run the bash script `make_slurm_files.sh`. It will generate the slurm files for the experiments and five scripts to run them.

In order to schedule the slurm scripts, go to folder `tgindex/cmake-build-release` and run the scripts

```
schedule_all_exps.sh
schedule_all_greedy.sh
schedule_all_closeness.sh
schedule_all_dynamic.sh
schedule_all_interval.sh
```

The results will be written in the `.out` files.


You can run `parse_result.sh` in the folder `scripts/parser` to generate `.csv` files containing the results.